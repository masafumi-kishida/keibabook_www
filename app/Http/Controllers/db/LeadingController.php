<?php

namespace App\Http\Controllers\db;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $message)
    {
      //
      //$year = date('Y');
      $year = $request->input('year', date('Y'));
      $method = $request->method();
      switch ($message) {
        case 'leadingjockey':
          return view ('db/leadingjockey', [
            'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '0', 'syubetu' => '00']),
            'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '1', 'syubetu' => '00'])
            ]);
          break;
        case 'leadingtrainer':
          return view ('db/leadingjockey', [
            'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '0', 'syubetu' => '01']),
            'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '1', 'syubetu' => '01'])
            ]);
          break;
        case 'leadingsire':
          return view ('db/leadingsire', [
            'leadingkisyu_kansai' => view ('db/leadingsire_list',['nen' => $year, 'syubetu' => '02']),
            ]);
          break;
        case 'rankingbanushi':
          return view ('db/rankingbanushi', [
            'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => $year, 'syubetu' => '00']),
            ]);
          break;
        case 'rankingseisansya':
          return view ('db/rankingbanushi', [
            'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => $year, 'syubetu' => '01']),
            ]);
          break;
        case 'umasearch':
          // 馬検索の処理
          break;
        default:
          # code...
          break;
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sub = "create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $year = $request->year;
      switch ($message) {
        case 'leadingjockey':
          return view ('db/leadingjockey', [
              'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '0', 'syubetu' => '00']),
              'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '1', 'syubetu' => '00'])
              ]);
          break;
        case 'leadingtrainer':
          return view ('db/leadingjockey', [
              'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '0', 'syubetu' => '01']),
              'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => $year, 'touzai' => '1', 'syubetu' => '01'])
              ]);
          break;
        case 'leadingsire':
          return view ('db/leadingsire', [
              'leadingkisyu_kansai' => view ('db/leadingsire_list',['nen' => $year, 'syubetu' => '02']),
              ]);
          break;
        case 'rankingbanushi':
          return view ('db/rankingbanushi', [
              'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => $year, 'syubetu' => '00']),
              ]);
          break;
        case 'rankingseisansya':
          return view ('db/rankingbanushi', [
              'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => $year, 'syubetu' => '01']),
              ]);
          break;
        default:
          # code...
          break;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($message)
    {

      //
      switch ($message) {
        case 'leadingjockey':
          return view ('db/leadingjockey', [
              'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => date('Y'), 'touzai' => '0', 'syubetu' => '00']),
              'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => date('Y'), 'touzai' => '1', 'syubetu' => '00'])
              ]);
          break;
        case 'leadingtrainer':
          return view ('db/leadingkisyu', [
              'leadingkisyu_kansai' => view ('db/leadingjockey_list',['nen' => date('Y'), 'touzai' => '0', 'syubetu' => '01']),
              'leadingkisyu_kantou' => view ('db/leadingjockey_list',['nen' => date('Y'), 'touzai' => '1', 'syubetu' => '01'])
              ]);
          break;
        case 'leadingsire':
          return view ('db/leadingsire', [
              'leadingkisyu_kansai' => view ('db/leadingsire_list',['nen' => date('Y'), 'syubetu' => '02']),
              ]);
          break;
        case 'rankingbanushi':
          return view ('db/rankingbanushi', [
              'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => date('Y'), 'syubetu' => '00']),
              ]);
          break;
        case 'rankingseisansya':
          return view ('db/rankingbanushi', [
              'leadingkisyu_kansai' => view ('db/rankingbanushi_list',['nen' => date('Y'), 'syubetu' => '01']),
              ]);
          break;
        case 'umasearch':
          // 馬検索の処理
          break;
        default:
          # code...
          break;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sub = "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $sub = "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sub = "destroy";
    }
}
