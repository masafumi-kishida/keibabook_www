<?php

//--------------------------------------
// helo テスト用Controllers
//--------------------------------------

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class HeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $method = "index";

        $html = Redis::get('helovalue');
        if (isset($html)) {
          return $html;
        } else {
          $res = $request->input('id');
          if (isset($res)) {
            $message = $res;
          } else {
            $message = 'Hello!';
          }
          $vv = view('test.helo', compact("message"));
          Redis::set('helovalue', $vv);
          return $vv;
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $method = "create";

    }
    public function create2()
    {
        //
        $method = "create2";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     //
    public function store(Request $request)
    {
        //
        $method = "store";
        $message = $request->input('str');
        return view('test.helo', compact("message"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id='', $id2='')
    {
        //
        $method = "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $method = "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $method = "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $method = "destroy";
    }
}
