<?php

namespace App\Http\Controllers\cyuou;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class CyuouController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/にアクセスされた場合
    public function index()
    {

        return view('cyuou/top', ['hiduke' => '20170320']);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/createにアクセスされた場合
    public function create()
    {
        //
        $message = 'create()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    // postでcyuou/にアクセスされた場合
    public function store(Request $request)
    {
        //
        $message = 'store()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/messageにアクセスされた場合
    public function show($message)
    {
        $hour = Carbon::now()->format('H');
        if($hour > '17'){
            $day = Carbon::tomorrow()->format('Ymd');
        }else {
            $day = Carbon::today()->format('Ymd');
        }
        //
        //$message = 'show()が呼ばれ：パラメータは'.$message;
        switch ($message) {
            case 'top':
            return view('cyuou/top', ['hiduke' => '20170320']);
            case 'nittei':
            return view('cyuou/nittei', ['negahi' => '20170320', 'welflg' => '0','race' => '11']);
            break;
            case 'odds':
            return view('cyuou/nittei', ['negahi' => '20170320', 'welflg' => '0','race' => '10']);
            break;
            case 'cyokyo':
            // 調教
            break;
            case 'paddok':
            // パドック
            break;
            case 'girigiri':
            // ぎりぎり情報
            break;
            default:
            # code...
            break;
        }

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/message/editにアクセスされた場合
    public function edit($id)
    {
        //
        $message = 'edit()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // putまたはpatchでcyuou/messageにアクセスされた場合
    public function update(Request $request, $id)
    {
        //
        $message = 'update()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // deleteでhello/messageにアクセスされた場合
    public function destroy($id)
    {
        //
        $message = 'destroy()が呼ばれた';
        return view('helo', compact('message'));
    }
}
