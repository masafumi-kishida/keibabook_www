<?php

namespace App\Http\Controllers\cyuou;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class TokubetuController extends Controller
{
    public function index($message)
    {
        $hour = Carbon::now()->format('H');
        if($hour > '17'){
            $day = Carbon::tomorrow()->format('Ymd');
        }else {
            $day = Carbon::today()->format('Ymd');
        }
        $nen = substr($message, 0, 4);
        $kai = substr($message, 4, 2);
        $basyocd = substr($message, 6, 2);
        $kaisai = substr($message, 8, 2);
        $race = substr($message, 10, 2);
        //
        //$message = 'show()が呼ばれ：パラメータは'.$message;
        return view('cyuou/tokubetu', ['nen' => $nen, 'kai' => $kai, 'basyocd' => $basyocd, 'kaisai' => $kaisai, 'race' => $race, 'welflg' => '0','race' => $race, 'basyocd' => $basyocd]);

    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/createにアクセスされた場合
    public function create(Request $request)
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    // postでcyuou/にアクセスされた場合
    public function store(Request $request)
    {

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/messageにアクセスされた場合
    public function show($message)
    {
        $hour = Carbon::now()->format('H');
        if($hour > '17'){
            $day = Carbon::tomorrow()->format('Ymd');
        }else {
            $day = Carbon::today()->format('Ymd');
        }
        $nen = substr($message, 0, 4);
        $kai = substr($message, 4, 2);
        $basyocd = substr($message, 6, 2);
        $kaisai = substr($message, 8, 2);
        $race = substr($message, 10, 2);
        //
        //$message = 'show()が呼ばれ：パラメータは'.$message;
        return view('cyuou/tokubetu', ['nen' => $nen, 'kai' => $kai, 'basyocd' => $basyocd, 'kaisai' => $kaisai, 'race' => $race, 'welflg' => '0','race' => $race, 'basyocd' => $basyocd]);

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // getでcyuou/message/editにアクセスされた場合
    public function edit($id)
    {
        //
        $message = 'edit()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // putまたはpatchでcyuou/messageにアクセスされた場合
    public function update(Request $request, $id)
    {
        //
        $message = 'update()が呼ばれた';
        return view('helo', compact('message'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    // deleteでhello/messageにアクセスされた場合
    public function destroy($id)
    {
        //
        $message = 'destroy()が呼ばれた';
        return view('helo', compact('message'));
    }
    //
    //
}
