<?php

//---------------------------------------------------
//  keibabook_www 中央競馬コンテンツ処理
//---------------------------------------------------
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CyuouController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // getでcyuou/で呼ばれた場合
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // getでcyuou/createで呼ばれた場合
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   // postでcyuou/で呼ばれた場合
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // getでcyuou/{id}で呼ばれた場合
    public function show($id)
    {


        //
        switch ($id) {
          case 'syutuba':
            // 出馬表
            
            break;
          case 'odds':
            // オッズ
            break;
          case 'cyokyo':
            // 調教
            break;
          case 'paddok':
            // パドック
            break;
          case 'girigiri':
            // ぎりぎり情報
            break;
          case 'cyokuzen':
            // 直前情報
            break;
          case 'kako':
            // 過去出馬
            break;
          case 'cpu':
            // ｃｐｕ予想
            break;
          case 'danwa':
            // 談話
            break;
          case 'syoin':
            // 勝因
            break;
          case 'taisen':
            // 対戦
            break；
          case 'speed':
            // スピード指数
            break;
          case 'rote':
            // ローテーション
            break;
          case 'zenkouhan':
            // 前後半
            break;
          case 'kyojost':
            // 今日の旗手厩舎
            break;
          case 'memo':
            // Ｍｙメモ
            break;
          case 'denmalab':
            // 究極分析
            break;
          case 'nrpaperpdf':
            // 野力表（ＰＤＦ）
            break;
          case 'nrpaperhtml':
            // 能力表（HTML)
            break;
          case 'photopaddok':
            // フォトパドック
            break;
          case 'cyokyogo':
            // 調教後の馬体重
            break;
          case 'tokusyu':
            // 特集ページ
            break;
          case 'seiseki':
            // レース結果
            break;
          default:
            //
            break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
