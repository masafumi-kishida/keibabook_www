<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Tableconvert extends Facade {

  protected static function getFacadeAccessor() {
    return 'tableconvert';             // バインドキーを返す。
  }

}
