<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTantousyaSetteiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TantousyaSettei', function (Blueprint $table) {
            $table->string('syozokucd', 1);
            $table->string('syainid', 2);
            $table->text('genkoumei1');
            $table->text('hyouji1');
            $table->text('hyouji2');
            $table->text('link');
            $table->integer('jyunban');
            $table->string('touzai', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TantousyaSettei');
    }
}
