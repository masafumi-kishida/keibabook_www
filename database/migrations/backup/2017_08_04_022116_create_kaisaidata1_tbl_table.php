<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaisaidata1TblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kaisaidata1_tbl', function (Blueprint $table) {
            $table->string('negahi', 8);
            $table->string('ynegahi', 8);
            $table->string('clfflg', 1);
            $table->string('nen', 4);
            $table->string('kai', 2);
            $table->string('basyocd', 2);
            $table->string('kaisai', 2);
            $table->string('active', 4);
            $table->integer('syume');
            $table->string('welflg', 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kaisaidata1_tbl');
    }
}
