<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//　トップページ
Route::resource('index', 'IndexController');

//--------------------------------
// 中央競馬
//--------------------------------
// 出馬一覧表
Route::get('cyuou',  'cyuou\CyuouController@index');
// 出馬レース単位
Route::get('cyuou_syutuba/{race}', 'cyuou\SyutubaController@show');
// 木曜出馬
Route::get('cyuou_thursday/{race}', 'cyuou\ThursdayController@show');
// 想定馬
Route::get('cyuou_sotei/{race}', 'cyuou\SoteiController@show');
// 特別登録
Route::get('cyuou_tokubetu/{race}', 'cyuou\TokubetuController@show');
// オッズ
Route::get('cyuou_odds/{race}', 'cyuou\OddsController@index');
// 調教
Route::get('cyuou_cyokyo/{race}', 'cyuou/CyokyogoController@index');
// パドック
Route::get('cyuou_paddok{race}', 'cyuou/PaddokController@index');
// ギリギリ情報
Route::get('cyuou_girigiri{race}', 'cyuou/GirigiriController@index');
// 直前情報
Route::get('cyuou_cyokuzen/{race}', 'cyuou/CyokuzenController@index');
// 過去一覧
Route::get('cyuou_kako/{race}', 'cyuou/KakoController@index');
// cpu予想
Route::get('cyuou_cpu/{race}', 'cyuou/CpuController@index');
// 談話
Route::get('cyuou_danwa/{race}', 'cyuou/DanwaController@index');
// 勝因敗因
Route::get('cyuou_syoin/{race}', 'cyuou/SyoinController@index');
// 対戦
Route::get('cyuou_taisen/{race}', 'cyuou/TaisenController@index');
// スピード指数
Route::get('cyuou_speed/{race}', 'cyuou/SpeedController@index');
// ローテーション
Route::get('cyuou_rote/{race}', 'cyuou/RoteController@index');
// 前後半タイム
Route::get('cyuou_zenkouhan/{race}', 'cyuou/ZenkouhanController@index');
// 今日の騎手厩舎
Route::get('cyuou_kyojost/{race}', 'cyuou/KyojostController@index');
// Myメモ
Route::get('cyuou_memo/{race}', 'cyuou/MemoController@index');
// 究極分析
Route::get('cyuou_denmalab/{race}', 'cyuou/DenmalabController@index');
// 能力表pdf
Route::get('cyuou_nrpaperpdf/{race}', 'cyuou/NrpaperpdfController@index');
// 能力表html
Route::get('cyuou_nrpaperhtml/{race}', 'cyuou/NrpaperhtmlController@index');
// パドック情報
Route::get('cyuou_photopaddok{race}', 'cyuou/PhotopaddokController@index');
// 調教後の馬体重
Route::get('cyuou_cyokyogo{race}', 'cyuou/CyokyogoController@index');
// 特集号へ
Route::get('cyuou_tokusyu/{race}', 'cyuou/TokusyuController@index');
// レース結果
Route::get('cyuou_seiseki/{race}', 'cyuou/SeisekiController@index');

// 地方競馬
Route::get('chihou', 'ChihouController@index');

// 海外競馬
Route::get('kaigai', 'KaigaiController@index');

// ニュース
Route::resource('news', 'newsController@index');

//----------------------------------
// データ検索
//----------------------------------
// リーディング
Route::resource('leading', 'db\LeadingController');

//-----------------------------------
// コラム
//-----------------------------------
//
Route::resource('weeklylog', 'column\WeeklylogController');
// 狙い馬
Route::get('neraiuma/{bas?}', 'column\NeraiumaController@index');
//
Route::resource('oikiritokuse', 'column\OikiritokuseController');
//
Route::resource('oikiripikaichi', 'column\OikiripikaichiController');

// ネット新聞
Route::resource('netpaper', 'NetpaperController');

// My馬
Route::resource('myuma', 'MyumaController');

// アミューズメント
Route::resource('amu', 'AmuController');

// せり
Route::resource('seri', 'SeriController');

// サポート
Route::resource('support', 'SupportController');



// テスト用
Route::resource('torm', 'TormController');  // ormテスト用
//Route::resource('helo', 'HeloController');  // heloテスト用
Route::get('helo', 'HeloController@index');  // heloテスト用
Route::get('helo/{id?}/{id2?}', 'HeloController@show');  // heloテスト用
Route::get('/', function () {
    return view('welcome');
});
