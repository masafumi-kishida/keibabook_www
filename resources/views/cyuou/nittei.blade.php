
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
出馬
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
<link href="/keibabook-www/css/reset.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/clear.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/global.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
{{-- ホームactive --}}
{{ $globalmenu_cyuou = "active" }}
{{ $globalnavi_nittei = "active" }}
@include('common.header')
@endsection

@section('content')
<!-- お知らせ -->
<div class="oshirase clearfix">

    <ul class="">
        <li class="midashi">速報</li>
        <li class="title"><a href="/">5/21（日）東京10Rのレース結果が確定しました。</a></li>
    </ul><!-- /. -->

    <p class="">

    </p>

</div><!-- /.oshirase -->

<?php

//カーボン
use App\KAISAIDATA1_TBL;
use Carbon\Carbon;

$week_str_list = array( '日', '月', '火', '水', '木', '金', '土');
$entrys = App\kaisaitest_tbl::where('active', '今週')
->orderBy('negahi')
->get();
$datej = '0';
foreach($entrys as $entry){
    $date = Carbon::parse($entry->negahi);
    if ($date == $datej){
        continue;
    }
    $datej = $date;
    $monday = $date->format('n/j');
    $week =  $date->dayOfWeek;
    $weeklist = $week_str_list[$week];
    $ymd = $date->format('Ymd');
    $dateonly[] = array('0' => $monday, '1' => $weeklist, '2' => $ymd);
}
$entrys = App\kaisaitest_tbl::where('negahi', $negahi)
->where('welflg', '1')
->get();
foreach($entrys as $entry){
    $basyo2 = $entry->basyoname;
    $wel = $entry->welflg;
    $keibajyo[] = array('0' => $basyo2, '1' => $wel);
}
$entrys = App\kaisaitest_tbl::where('negahi', $negahi)
->wherein('welflg', ['0', '2'])
->get();
foreach($entrys as $entry){
    $basyo2 = $entry->basyoname;
    $wel = $entry->welflg;
    $keibajyo[] = array('0' => $basyo2, '1' => $wel);
}

$races = App\racetest_tbl::where('negahi', $negahi)
->where('welflg', '0')
->orderBy('race')
->get();
foreach($races as $racea){
    $racenum = $racea->race;
    $racenum = ltrim($racenum, '0');
    $raceban[] = $racenum;
}

$raceb = App\racetest_tbl::where('negahi', $negahi)
->where('welflg', '0')
->where('race', $race)
->first();
$kai = ltrim($raceb->kai, '0');
$basyo = $raceb->basyoname;
$kaisai = ltrim($raceb->kaisai, '0');
$racenum = ltrim($race, '0');
$tokuname = $raceb->tokuname;
$nenjyo = $raceb->syubetu;
$jyoken = $raceb->jyoken;
$win5 = $raceb->win5;
$hyojirace['0'] = $win5;
$hyojirace['1'] = $kai;
$hyojirace['2'] = $basyo;
$hyojirace['3'] = $kaisai;
$hyojirace['4'] = $racenum;
$hyojirace['5'] = $tokuname;
switch ($nenjyo) {
    case '00':
    $hyojirace['6'] = '3歳';
    break;
    case '02':
    $hyojirace['6'] = '4歳以上';
    break;
    case '15':
    $hyojirace['6'] = '2歳';
    break;
    case '16':
    $hyojirace['6'] = '3歳以上';
    break;
    case '04':
    $hyojirace['6'] = '4歳以上障害';
    break;
    case '18':
    $hyojirace['6'] = '3歳以上障害';
    break;
    default:
    $hyojirace['6'] = '';
    break;
}
switch ($jyoken) {
    case '05':
    $hyojirace['7'] = '500万下';
    break;
    case '10':
    $hyojirace['7'] = '1000万下';
    break;
    case '16':
    $hyojirace['7'] = '1600万下';
    break;
    case '50':
    $hyojirace['7'] = 'オープン';
    break;
    case '51':
    $hyojirace['7'] = '新馬';
    break;
    case '53':
    $hyojirace['7'] = '未勝利';
    break;
    default:
    $hyojirace['7'] = '';
    break;
}
$tokubetu = $raceb->tokubetu;
$sotei = $raceb->sotei;
$mokuyou = $raceb->mokuyou;
$kakutei = $raceb->kakutei;
$odds = $raceb->odds;
$cyokyo = $raceb->cyokyo;
$padock = $raceb->padock;
$girigiri = $raceb->girigiri;
$tm = $raceb->tm;
$kakoichiran = $raceb->kakoichiran;
$cpu = $raceb->cpu;
$danwa = $raceb->danwa;
$point = $raceb->point;
$syouin = $raceb->syouin;
$taisen = $raceb->taisen;
$speed = $raceb->speed;
$rotetion = $raceb->rotetion;
$zenkouhan = $raceb->zenkouhan;
$kyojost = $raceb->kyojost;
$memo = $raceb->memo;
$denmalab = $raceb->denmalab;
$nouryoku_pdf = $raceb->nouryoku_pdf;
$nouryoku_html = $raceb->nouryoku_html;
$tokusyu = $raceb->tokusyu;
$seiseki = $raceb->seiseki;

$racec = App\kakuteisyutuba1::where('negahi', $negahi)
->where('welflg', '1')
->where('race', $race)
->first();
$hasou = $racec->hasou;
$hyojirace['8'] = substr($hasou, 0, 2);
$hyojirace['9'] = substr($hasou, 2, 2);
$kyori = $racec->kyori;
$sibadaf = $racec->sibada;
$migihidaf = $racec->migihida;
$utisotof = $racec->utisoto;
switch($sibadaf){
    case '0':
    $sibada = 'ダ';
    break;
    case '1':
    $sibada = '芝';
    break;
}
switch($migihidaf){
    case '0':
    $migihida = '右';
    break;
    case '1':
    $migihida = '左';
    break;
    case '2':
    $migihida = '直';
    break;
}
switch($utisotof){
    case '1':
    $utisoto = '外';
    break;
    case '2':
    $utisoto = '外→内';
    break;
    case '3':
    $utisoto = 'タスキ';
    break;
    case '4':
    $utisoto = '大障害';
    break;
    case '5':
    $utisoto = '内2周';
    break;
    default:
    $utisoto = '';
    break;
}
$racejyoho['0'] = $kyori.'m';
$racejyoho['1'] = '('.$sibada.'・'.$migihida.$utisoto.')';

$raced = App\BANGUMITEST_TBL::where('negahi', $negahi)
->where('welflg', '0')
->where('yrace', $race)
->first();
$syokin1 = $raced->syokin1;
$syokin2 = $raced->syokin2;
$syokin3 = $raced->syokin3;
$syokin4 = $raced->syokin4;
$syokin5 = $raced->syokin5;
$racejyoho['2'] = substr($syokin1, 0, -3);
$racejyoho['3'] = substr($syokin2, 0, -3);
$racejyoho['4'] = substr($syokin3, 0, -3);
$racejyoho['5'] = substr($syokin4, 0, -3);
$racejyoho['6'] = substr($syokin5, 0, -3);

$racee = App\recotime_tbl::where('basyocd', '05')
->where('sibada', $sibadaf)
->where('migihida', $migihidaf)
->where('utisoto', $utisotof)
->where('kyori', $kyori)
->orderby('negahi', 'desc')
->first();
$rectime = $racee->rectime;
$min = substr($rectime, 0, 1);
$sec = substr($rectime, 1, 2);
$dem = substr($rectime, 3, 1);
$racejyoho['7'] = $min.'.'.$sec.'.'.$dem;

$umas = App\kakuteisyutuba2::where('negahi', $negahi)
->where('welflg', '1')
->where('race', $race)
->orderby('umaban')
->get();
$wak0 = '0';
foreach ($umas as $uma) {
    $wak = $uma->wakban;
    $ban = $uma->umaban;
    $ban = ltrim($ban, '0');
    $bamei = $uma->kbamei;
    $sex = $uma->seibet;
    switch($sex){
        case '0':
        $seirei = '牡';
        break;
        case '1':
        $seirei = '牝';
        break;
        case '2':
        $seirei = 'セ';
        break;
    }
    $nenrei = $uma->nenrei;
    $nenrei = ltrim($nenrei, '0');
    $seirei = $seirei.$nenrei;
    $bagu = $uma->blink;
    if ($bagu == '1'){
        $bagu = 'B';
    }
    $kinryo = $uma->kinryo;
    $seisu = substr($kinryo, 0, 2);
    $hasu = substr($kinryo, 2);
    $kinryo = $seisu.'.'.$hasu;
    $bataijyu = $uma->bataijyu;
    $zougen = $uma->zougen;
    $bataijyu = $bataijyu.'('.$zougen.')';
    $genryo = $uma->minarai;
    switch($genryo){
        case '0':
        $genryo = '▲';
        break;
        case '1':
        $genryo = '△';
        break;
        case '2':
        $genryo = '☆';
        break;
    }
    $bkisyu = $uma->bkisyu;
    $nori = $uma->kawarif;
    if ($nori == '1'){
        $nori = '(替)';
    }else{
        $nori = '';
    }
    $kisyu = $nori.$genryo.$bkisyu;
    $trainer = $uma->bkyusya;
    $touzai = $uma->touzai;
    switch($touzai){
        case '1':
        $touzai = '(栗東)';
        break;
        case '2':
        $touzai = '(美浦)';
        break;
        default:
        $touzai = '(他)';
        break;
    }
    $kyusya = $trainer.$touzai;
    $umalist[] = array('0' => $wak, '1' => $ban, '2' => $bamei, '3' => $seirei, '4' => $bagu, '5' => $kinryo, '6' => $bataijyu, '7' => $kisyu, '8' => $kyusya);
}
?>

<div id="container">
    <!-- レースの切り替え -->
    <div id="flex_container_top" class="clearfix">
        <div class="raceindex">
            <ul class="negahi clearfix">
                @foreach($dateonly as $aaa)
                @if($aaa[2] == $negahi)
                <li class="active"><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @else
                <li class=""><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @endif
                @endforeach
            </ul>

            <ul class="keibajyo clearfix">
                @foreach($keibajyo as $basyo2)
                @if($basyo2[1] == $welflg)
                <li class="active"><a href="/" title=test>{{ $basyo2[0] }}</a></li>
                @else
                <li class=""><a href="/" title=test>{{ $basyo2[0] }}</a></li>
                @endif
                @endforeach
            </ul><!-- /.keibajyo -->
            <ul class="race clearfix">
                @foreach($raceban as $racenum)
                @if($racenum == $race)
                <li class="active"><a href="/" title=test>{{ $racenum }}R</a></li>
                @else
                <li class=""><a href="/" title=test>{{ $racenum }}R</a></li>
                @endif
                @endforeach
            </ul><!-- /.race -->
        </div>
        <div class="racename">
            @if(isset($hyojirace[0]))
            <p>WIN5対象レース</p>
            @endif
            <p>{{ $hyojirace[1] }}回{{$hyojirace[2]}}{{$hyojirace[3]}}日目{{$hyojirace[4]}}R</p>
            <p>{{ $hyojirace[5] }}</p>
            <p>{{ $hyojirace[6] }}{{$hyojirace[7]}}</p>
            <p>発走{{ $hyojirace[8] }}:{{$hyojirace[9]}}</p>
        </div><!-- /.menuindex -->
        <div class="racejyoken">
            <p>{{ $racejyoho[0] }}</p>
            <p>{{ $racejyoho[1] }}</p>
        </div><!-- /.menuindex -->
        <div class="racesyokin">
            <p>①{{ $racejyoho[2] }}万円</p>
            <p>②{{ $racejyoho[3] }}万円</p>
            <p>③{{ $racejyoho[4] }}万円</p>
            <p>④{{ $racejyoho[5] }}万円</p>
            <p>⑤{{ $racejyoho[6] }}万円</p>
        </div><!-- /.menuindex -->
        <div class="racerec">
            <p>レコード{{ $racejyoho[7] }}</p>
        </div><!-- /.menuindex -->
    </div><!-- /#flex_container_top -->
</div>


ここにお待たせ



<!-- ここからコンテンツ -->
<div id="flex_container" class="clearfix">

    <div id="fullcontent">

        <!-- コンテンツの切り替え -->
        <!-- ここは可変で出力して下さい -->
        <div class="menuindex">
            <ul class="cyuou clearfix">
                @if (isset($tokubetu))
                <li class="active"><a href="/testsite/www/cyuou/syutuba.php" title="出馬表">出馬表</a></li>
                @else
                <li class="">出馬表</li>
                @endif
                @if (isset($odds))
                <li class=""><a href="/testsite/www/cyuou/odds.php" title="オッズ">オッズ</a></li>
                @else
                <li class="">オッズ</li>
                @endif
                @if (isset($cyokyo))
                <li class=""><a href="/testsite/www/cyuou/cyokyo.php" title="調教">調教</a></li>
                @else
                <li class="">調教</li>
                @endif
                @if (isset($paddok))
                <li class=""><a href="/testsite/www/cyuou/paddok.php" title="パドック情報">パドック情報</a></li>
                @else
                <li class="">パドック情報</li>
                @endif
                @if (isset($girigiri))
                <li class=""><a href="/testsite/www/cyuou/girigiri.php" title="ギリギリ情報">ギリギリ情報</a></li>
                @else
                <li class="">ギリギリ情報</li>
                @endif
                @if (isset($template))
                <li class=""><a href="/testsite/www/cyuou/cyokuzen.php" title="トラックマン直前情報">TM直前情報</a></li>
                @else
                <li class="">TM直前情報</li>
                @endif
                @if (isset($kakoichiran))
                <li class=""><a href="/testsite/www/cyuou/kako.php" title="過去一覧">過去一覧</a></li>
                @else
                <li class="">過去一覧</li>
                @endif
                @if (isset($cpu))
                <li class=""><a href="/testsite/www/cyuou/cpu.php" title="コンピュータ予想">コンピュータ予想</a></li>
                @else
                <li class="">コンピュータ予想</li>
                @endif
                @if (isset($danwa))
                <li class=""><a href="/testsite/www/cyuou/danwa.php" title="厩舎の話">厩舎の話</a></li>
                @else
                <li class="">厩舎の話</li>
                @endif
                @if (isset($syouin))
                <li class=""><a href="/testsite/www/cyuou/syoin.php" title="前走の勝因敗因">前走の勝因敗因</a></li>
                @else
                <li class="">前走の勝因敗因</li>
                @endif
                @if (isset($taisen))
                <li class=""><a href="/testsite/www/cyuou/taisen.php" title="対戦成績">対戦成績</a></li>
                @else
                <li class="">対戦成績</li>
                @endif
                @if (isset($speed))
                <li class=""><a href="/testsite/www/cyuou/speed.php" title="スピード指数">スピード指数</a></li>
                @else
                <li class="">スピード指数</li>
                @endif
                @if (isset($rotetion))
                <li class=""><a href="/testsite/www/cyuou/rote.php" title="ローテーション">ローテーション</a></li>
                @else
                <li class="">ローテーション</li>
                @endif
                @if (isset($zenkouhan))
                <li class=""><a href="/testsite/www/cyuou/zenkouhan.php" title="前後半タイム">前後半タイム</a></li>
                @else
                <li class="">前後半タイム</li>
                @endif
                @if (isset($kyojost))
                <li class=""><a href="/testsite/www/cyuou/kyojost.php" title="今日の騎手厩舎">今日の騎手厩舎</a></li>
                @else
                <li class="">今日の騎手厩舎</li>
                @endif
                @if (isset($memo))
                <li class=""><a href="/testsite/www/cyuou/memo.php" title="Ｍｙメモ">Ｍｙメモ</a></li>
                @else
                <li class="">Ｍｙメモ</li>
                @endif
                @if (isset($denmalab))
                <li class=""><a href="/testsite/www/cyuou/denmalab.php" title="究極分析">究極分析</a></li>
                @else
                <li class="">究極分析</li>
                @endif
                @if (isset($nouryoku_pdf))
                <li class=""><a href="/testsite/www/cyuou/nrpaperpdf.php" title="能力表（PDF）">能力表（PDF）</a></li>
                @else
                <li class="">能力表（PDF）</li>
                @endif
                @if (isset($nouryoku_html))
                <li class=""><a href="/testsite/www/cyuou/nrpaperhtml.php" title="能力表（Html）">能力表（Html）</a></li>
                @else
                <li class="">能力表（Html）</li>
                @endif
                @if (isset($photopaddoc))
                <li class=""><a href="/testsite/www/cyuou/photopaddok.php" title="PHOTOパドック">PHOTOパドック</a></li>
                @else
                <li class="">&nbsp;</li>
                @endif
                @if (isset($tokusyu))
                <li class=""><a href="/testsite/www/cyuou/tokusyu.php" title="{{$hyojirace[5]}}特集へ">{{$hyojirace[5]}}特集へ</a></li>
                @else
                <li class="">&nbsp;</li>
                @endif
                <li class="">&nbsp;</li>
                <li class="">&nbsp;</li>
                @if (isset($seiseki))
                <li class=""><a href="/testsite/www/cyuou/seiseki.php" title="レース結果">レース結果</a></li>
                @else
                <li class="">レース結果</li>
                @endif
            </ul>
        </div><!-- /.menuindex -->


        <!-- 特別登録馬・想定馬・枠順確定前出馬・確定出馬の切り替え -->
        <!-- ここは可変で出力して下さい -->
        <div class="menuindex">
            <ul class="cyuou clearfix">
                @if (isset($tokubetu))
                <li class=""><a href="/testsite/www/cyuou/tokubetu.php" title="特別登録馬">特別登録馬</a></li>
                @else
                <li class="">特別登録馬</li>
                @endif
                @if (isset($sotei))
                <li class=""><a href="/testsite/www/cyuou/sotei.php" title="想定馬">想定馬</a></li>
                @else
                <li class="">想定馬</li>
                @endif
                @if (isset($mokuyou))
                <li class=""><a href="/testsite/www/cyuou/wakujunkakuteimae.php" title="枠順確定前出馬">枠順確定前出馬</a></li>
                @else
                <li class="">枠順確定前出馬</li>
                @endif
                @if (isset($kakutei))
                <li class="active"><a href="/testsite/www/cyuou/syutuba.php" title="確定出馬">確定出馬</a></li>
                @else
                <li class="">確定出馬</li>
                @endif
            </ul>
        </div><!-- /.menuindex -->
        <div class="section">

            <table class="syutuba width100" id="syutuba_sort_table">
                <thead>
                    <tr>
                        <th>枠<br>番</th>
                        <th>馬<br>番</th>
                        <th>馬名</th>
                        <th>性齢</th>
                        <th>馬<br>具</th>
                        <th>重<br>量</th>
                        <th>馬体重</th>
                        <th>騎手</th>
                        <th>厩舎</th>
                        <th>単勝</th>
                        <th>人気</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($umalist as $uma)
                    <tr>
                        @for($i = 0; $i < 9; $i++)
                        <th>{{$uma[$i]}}</th>
                        @endfor
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- 見解,展開,推理のキー,トラックマン予想連番 -->
        <div class="flex_syutuba clearfix">

            <!-- 左側 -->
            <div class="flex_syutuba_left">

                <!-- 見解 -->
                <div class="boxsection">

                    <p class="title">見解</p>
                    <p>ここに見解</p>

                </div><!-- /.boxsection -->

                <!-- 展開 -->
                <div class="boxsection">

                    <p class="title">展開</p>
                    <p>ここに展開</p>

                </div><!-- /.boxsection -->

                <!-- 推理のキー -->
                <div class="boxsection">

                    <p class="title">推理のキー</p>
                    <p>ここに推理のキー</p>

                </div><!-- /.boxsection -->

            </div><!-- /.flex_syutuba_left -->

            <!-- 右側 -->
            <div class="flex_syutuba_right">

                <!-- トラックマン予想連番 -->
                <div class="boxsection">

                    <p class="title">トラックマン予想連番</p>
                    <div class="renban">
                        <table class="renban width100">
                            <tbody>
                            @for($i = 0; $i < 5; $i++)
                                <tr>
                                    <td class="name">レイティング</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                    <td>1-2</td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                        <table class="renban width100">
                            <tbody>
                                <tr>
                                    <td class="name" rowspan="2">レイティング</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                </tr>
                                <tr>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                    <td>2-1</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="renban width100">
                            <tbody>
                                <tr>
                                    <td class="name" rowspan="2">Ｖ作戦</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                </tr>
                                <tr>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                    <td>1-3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div><!-- /.boxsection -->

            </div><!-- /.flex_syutuba_left -->

        </div><!-- /.flex_syutuba -->


        <!-- 追い切りピカイチ,厩舎回り特選,トビっきりこぼれ話,トジャンプ競走応援団 -->
        <!-- タブ切り替え -->
        <div class="section" style="コラム">

            <!-- タブメニュー -->
            <ul id="tab-menu">
                <li class="active" style="おいきり">追い切りピカイチ</li>
                <li style="厩舎">厩舎回り特選</li>
                <li style="トビ">トビっきりこぼれ話</li>
                <li style="ジャンプ">ジャンプ競走応援団</li>
            </ul><!-- /#tab-menu -->

            <!-- タブの中身 -->
            <div id="tab-box">
                <div class="active" style="おいきり">追い切りピカイチ</div>
                <div style="厩舎">厩舎回り特選</div>
                <div style="トビ">トビっきりこぼれ話</div>
                <div style="ジャンプ">ジャンプ競走応援団</div>
            </div><!-- /#tab-box -->

        </div><!-- /.section -->

        @endsection
