@extends('layouts.master')
@section('stylesheet')
	<link rel="stylesheet" href="/css/import.css"/>
@stop
@section('content')

<div id="container">

	<!-- レースの切り替え -->
	<div id="flex_container_top" class="clearfix">

		<div class="main">

		<!-- お知らせ -->
		<div class="oshirase clearfix">

			<ul class="">
			<li class="midashi">速報</li>
			<li class="title"><a href="/">5/21（日）東京10Rのレース結果が確定しました。</a></li>
			</ul><!-- /. -->

			<p class="">

			</p>

		</div><!-- /.oshirase -->


		<!-- レースインデックス -->
		<!-- ここは可変で出力して下さい -->
		<div class="raceindex">

			<ul class="negahi clearfix">
				<li class=""><a href="/" title="5/20(土)">5/20(土)</a></li>
				<li class="active"><a href="/" title="5/21(日)">5/21(日)</a></li>
				<li class=""><a href="/" title="5/27(土)">5/27(土)</a></li>
				<li class=""><a href="/" title="5/28(日)">5/28(日)</a></li>
				<li class=""><a href="/" title="6/4(日)">6/4(日)</a></li>
			</ul><!-- /.negahi -->

			<ul class="keibajyo clearfix">
				<li class="active"><a href="/" title="東京">東京</a></li>
				<li class=""><a href="/" title="京都">京都</a></li>
				<li class=""><a href="/" title="ローカル">ローカル</a></li>
			</ul><!-- /.keibajyo -->

			<ul class="race clearfix">
				<li class=""><a href="/" title="1R">1R</a></li>
				<li class=""><a href="/" title="2R">2R</a></li>
				<li class=""><a href="/" title="3R">3R</a></li>
				<li class=""><a href="/" title="4R">4R</a></li>
				<li class=""><a href="/" title="5R">5R</a></li>
				<li class=""><a href="/" title="6R">6R</a></li>
				<li class=""><a href="/" title="7R">7R</a></li>
				<li class=""><a href="/" title="8R">8R</a></li>
				<li class=""><a href="/" title="9R">9R</a></li>
				<li class=""><a href="/" title="10R">10R</a></li>
				<li class="active"><a href="/" title="11R">11R</a></li>
				<li class=""><a href="/" title="12R">12R</a></li>
			</ul><!-- /.race -->

		</div><!-- /.raceindex -->

		<!-- レース名などの表示 -->
		<!-- ここは可変で出力して下さい -->
		<div class="racename">
			<p>中京７日目11R</p>
			<p>桶狭間ステークス</p>
			<p>サラ３歳以上1600万下,(特指),(混),定量</p>
			<p>発走 15:35</p>
		</div><!-- /.menuindex -->
		</div><!-- /.main -->

		<div class="side">
			ここにお待たせ
		</div><!-- /.side -->
	</div><!-- /#flex_container_top -->



	<!-- ここからコンテンツ -->
	<div id="flex_container" class="clearfix">

		<div id="fullcontent">

			<!-- コンテンツの切り替え -->
			<!-- ここは可変で出力して下さい -->
			<div class="menuindex">
				<ul class="cyuou clearfix">
					<li class="active"><a href="/testsite/www/cyuou/syutuba.php" title="出馬表">出馬表</a></li>
					<li class=""><a href="/testsite/www/cyuou/odds.php" title="オッズ">オッズ</a></li>
					<li class=""><a href="/testsite/www/cyuou/cyokyo.php" title="調教">調教</a></li>
					<li class=""><a href="/testsite/www/cyuou/paddok.php" title="パドック情報">パドック情報</a></li>
					<li class=""><a href="/testsite/www/cyuou/girigiri.php" title="ギリギリ情報">ギリギリ情報</a></li>
					<li class=""><a href="/testsite/www/cyuou/cyokuzen.php" title="トラックマン直前情報">TM直前情報</a></li>
					<li class=""><a href="/testsite/www/cyuou/kako.php" title="過去一覧">過去一覧</a></li>
					<li class=""><a href="/testsite/www/cyuou/cpu.php" title="コンピュータ予想">コンピュータ予想</a></li>
					<li class=""><a href="/testsite/www/cyuou/danwa.php" title="厩舎の話">厩舎の話</a></li>
					<li class=""><a href="/testsite/www/cyuou/syoin.php" title="前走の勝因敗因">前走の勝因敗因</a></li>
					<li class=""><a href="/testsite/www/cyuou/taisen.php" title="対戦成績">対戦成績</a></li>
					<li class=""><a href="/testsite/www/cyuou/speed.php" title="スピード指数">スピード指数</a></li>
					<li class=""><a href="/testsite/www/cyuou/rote.php" title="ローテーション">ローテーション</a></li>
					<li class=""><a href="/testsite/www/cyuou/zenkouhan.php" title="前後半タイム">前後半タイム</a></li>
					<li class=""><a href="/testsite/www/cyuou/kyojost.php" title="今日の騎手厩舎">今日の騎手厩舎</a></li>
					<li class=""><a href="/testsite/www/cyuou/memo.php" title="Ｍｙメモ">Ｍｙメモ</a></li>
					<li class=""><a href="/testsite/www/cyuou/denmalab.php" title="究極分析">究極分析</a></li>
					<li class=""><a href="/testsite/www/cyuou/nrpaperpdf.php" title="能力表（PDF）">能力表（PDF）</a></li>
					<li class=""><a href="/testsite/www/cyuou/nrpaperhtml.php" title="能力表（Html）">能力表（Html）</a></li>
					<li class=""><a href="/testsite/www/cyuou/photopaddok.php" title="PHOTOパドック">PHOTOパドック</a></li>
					<li class=""><a href="/testsite/www/cyuou/cyokyogo.php" title="調教後の馬体重">調教後の馬体重</a></li>
					<li class=""><a href="/testsite/www/cyuou/tokusyu.php" title="○○○○特集へ">○○○○特集へ</a></li>
					<li class="">&nbsp;</li>
					<li class=""><a href="/testsite/www/cyuou/seiseki.php" title="レース結果">レース結果</a></li>
				</ul>
			</div><!-- /.menuindex -->


			<!-- 特別登録馬・想定馬・枠順確定前出馬・確定出馬の切り替え -->
			<!-- ここは可変で出力して下さい -->
			<div class="menuindex">
				<ul class="cyuou clearfix">
					<li class=""><a href="/testsite/www/cyuou/tokubetu.php" title="特別登録馬">特別登録馬</a></li>
					<li class=""><a href="/testsite/www/cyuou/sotei.php" title="想定馬">想定馬</a></li>
					<li class=""><a href="/testsite/www/cyuou/wakujunkakuteimae.php" title="枠順確定前出馬">枠順確定前出馬</a></li>
					<li class="active"><a href="/testsite/www/cyuou/syutuba.php" title="確定出馬">確定出馬</a></li>
				</ul>
			</div><!-- /.menuindex -->


			<!-- 出馬表 -->
			<div class="section">

				<table class="syutuba width100" id="syutuba_sort_table">
					<thead>
					<tr>
						<th>枠<br>番</th>
						<th>馬<br>番</th>
						<th>馬記号</th>
						<th>馬名</th>
						<th>展<br>開</th>
						<th>調<br>教</th>
						<th>性齢</th>
						<th>馬<br>具</th>
						<th>重<br>量</th>
						<th>騎手</th>
						<th>厩舎</th>
						<th>馬体重</th>
						<th>{{$tansyo}}</th>
						<th>人気</th>
						<th class="tmyoso">{{$yosouysa1}}</th>
						<th class="tmyoso">{{$yosouysa2}}</th>
						<th class="tmyoso">{{$yosouysa3}}</th>
						<th class="tmyoso">{{$yosouysacpu}}</th>
						<th>Rate</th>
					</tr>
					</thead>

					<tbody>
					@foreach($items as $item)
					<tr class="{{ $item->torikesi }}">
						<td class="waku"><p class="{{ $item->class_waku }}">{{ $item->wakuban }}</p></td>
						<td>{{ $item->umaban }}</td>
						<td>{{ $item->umakigou }}</td>
						<td class="left">{{ $item->kbamei }}</td>
						<td>{{ $item->tenkai }}</td>
						<td>{{ $item->cyokyomovie }}</td>
						<td>{{ $item->seirei }}</td>
						<td>{{ $item->bagu }}</td>
						<td>{{ $item->kinryo }}</td>
						<td class="left">{{ $item->kisyu }}</td>
						<td class="left">{{ $item->kyusya }}</a></td>
						<td class="left">{{ $item->bataiju }}</td>
						<td class="right">{{ $item->tansyo }}</td>
						<td class="right">{{ $item->ninki }}</td>
						<td>{{ $item->tmyoso1 }}</td>
						<td>{{ $item->tmyoso2 }}</td>
						<td>{{ $item->tmyoso3 }}</td>
						<td>{{ $item->tmyosocpu }}</td>
						<td class="right">{{ $item->rating }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>

			</div><!-- /.section -->

			<!-- 見解,展開,推理のキー,トラックマン予想連番 -->
			<div class="flex_syutuba clearfix">

				<!-- 左側 -->
				<div class="flex_syutuba_left">

					<!-- 見解 -->
					<div class="boxsection">

						<p class="title">見解</p>
						<p>{{$kenkai}}</p>

					</div><!-- /.boxsection -->

					<!-- 展開 -->
					<div class="boxsection">

						<p class="title">展開</p>
						<p>{{$tenkai}}</p>

					</div><!-- /.boxsection -->

					<!-- 推理のキー -->
					<div class="boxsection">

						<p class="title">推理のキー</p>
						<p>{{$suiri}}</p>

					</div><!-- /.boxsection -->

				</div><!-- /.flex_syutuba_left -->

				<!-- 右側 -->
				<div class="flex_syutuba_right">

					<!-- トラックマン予想連番 -->
					<div class="boxsection">

						<p class="title">トラックマン予想連番</p>
						<div class="renban">
							<table class="renban width100">
								<tbody>
								@foreach($items as $item)
									<tr>
										<td>{{ $item->tmname }}</td>
										<td>{{ $item->renban1 }}</td>
										<td>{{ $item->renban2 }}</td>
										<td>{{ $item->renban3 }}</td>
										<td>{{ $item->renban4 }}</td>
										<td>{{ $item->renban5 }}</td>
										<td>{{ $item->renban6 }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							<table class="renban width100">
								<tbody>
									<tr>
										<td class="name" rowspan="2">レイティング</td>
										<td>{{ $rating_renban1 }}</td>
										<td>{{ $rating_renban2 }}</td>
										<td>{{ $rating_renban3 }}</td>
										<td>{{ $rating_renban4 }}</td>
										<td>{{ $rating_renban5 }}</td>
										<td>{{ $rating_renban6 }}</td>
									</tr>
									<tr>
										<td>{{ $rating_renban7 }}</td>
										<td>{{ $rating_renban8 }}</td>
										<td>{{ $rating_renban9 }}</td>
										<td>{{ $rating_renban10 }}</td>
										<td>{{ $rating_renban11 }}</td>
										<td>{{ $rating_renban12 }}</td>
									</tr>
								</tbody>
							</table>
							<table class="renban width100">
								<tbody>
									<tr>
										<td class="name" rowspan="2">Ｖ作戦</td>
										<td>{{ $vsaku_renban1 }}</td>
										<td>{{ $vsaku_renban2 }}</td>
										<td>{{ $vsaku_renban3 }}</td>
										<td>{{ $vsaku_renban4 }}</td>
										<td>{{ $vsaku_renban5 }}</td>
										<td>{{ $vsaku_renban6 }}</td>
									</tr>
									<tr>
										<td>{{ $vsaku_renban7 }}</td>
										<td>{{ $vsaku_renban8 }}</td>
										<td>{{ $vsaku_renban9 }}</td>
										<td>{{ $vsaku_renban10 }}</td>
										<td>{{ $vsaku_renban11 }}</td>
										<td>{{ $vsaku_renban12 }}</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div><!-- /.boxsection -->

				</div><!-- /.flex_syutuba_left -->

			</div><!-- /.flex_syutuba -->


			<!-- 追い切りピカイチ,厩舎回り特選,トビっきりこぼれ話,トジャンプ競走応援団 -->
			<!-- タブ切り替え -->
			<div class="section" style="{{$sytle_column}}">

				<!-- タブメニュー -->
				<ul id="tab-menu">
					<li class="active" style="{{$sytle_column1}}">{{$column_title_1}}</li>
					<li style="{{$sytle_column2}}">{{$column_title_2}}</li>
					<li style="{{$sytle_column3}}">{{$column_title_3}}</li>
					<li style="{{$sytle_column4}}">{{$column_title_4}}</li>
				</ul><!-- /#tab-menu -->

				<!-- タブの中身 -->
				<div id="tab-box">
					<div class="active" style="{{$sytle_column1}}">{{$column_1}}</div>
					<div style="{{$sytle_column2}}">{{$column_2}}</div>
					<div style="{{$sytle_column3}}">{{$column_3}}</div>
					<div style="{{$sytle_column4}}">{{$column_4}}</div>
				</div><!-- /#tab-box -->

			</div><!-- /.section -->


		</div><!-- /#fullcontent -->

	</div><!-- /#flex_container -->

</div><!-- /#container -->


@stop
