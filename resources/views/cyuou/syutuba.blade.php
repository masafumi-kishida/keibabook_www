@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
出馬
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
<link href="/keibabook-www/css/reset.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/clear.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/global.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
{{-- ホームactive --}}
{{ $globalmenu_cyuou = "active" }}
{{ $globalnavi_nittei = "active" }}
@include('common.header')
@endsection

@section('content')
<!-- お知らせ -->
<div class="oshirase clearfix">
    <ul class="">
        <li class="midashi">速報</li>
        <li class="title"><a href="/">5/21（日）東京10Rのレース結果が確定しました。</a></li>
    </ul><!-- /. -->
    <p class="">
    </p>
</div><!-- /.oshirase -->
<?php
use Carbon\Carbon;
use App\Facades\Tableconvert;

$date = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$negahi = $date->negahi;

$entryday = App\kaisaidata1_tbl::where([
    ['negahi', '=', $negahi],
    ['basyocd', '=', $basyocd],
])
->first();
$syume = $entryday->syume;

$week_str_list = array( '日', '月', '火', '水', '木', '金', '土');

$entryday = App\kaisaidata1_tbl::where('syume', $syume)
->orderBy('negahi')
->get();
$datej = '0';
foreach($entryday as $entry){
    $date = Carbon::parse($entry->negahi);
    if ($date == $datej){
        continue;
    }
    $datej = $date;
    $monday = $date->format('n/j');
    $week =  $date->dayOfWeek;
    $weeklist = $week_str_list[$week];
    $ymd = $date->format('Ymd');
    $dateonly[] = array('0' => $monday, '1' => $weeklist, '2' => $ymd);
}

$entrybasyo = App\kaisaidata1_tbl::where('negahi', $negahi)
->where('welflg', '1')
->get();
foreach($entrybasyo as $entry){
    $kai1 = $entry->kai;
    $basyocode = $entry->basyocd;
    $kaisai1 = $entry->kaisai;
    $basyo = App\keibajyocd_t::where('keibajyocd', $basyocode)
    ->first();
    $basyo2 = $basyo->keibajyo2moji;
    $keibajyo[] = array('0' => $basyo2, '1' => $kai1, '2' => $basyocode, '3' => $kaisai1);
}

$entrybasyo = App\kaisaidata1_tbl::where('negahi', $negahi)
->wherein('welflg', ['0', '2'])
->orderby('welflg')
->get();
foreach($entrybasyo as $entry){
    $kai1 = $entry->kai;
    $basyocode = $entry->basyocd;
    $kaisai1 = $entry->kaisai;
    $basyo = App\keibajyocd_t::where('keibajyocd', $basyocode)
    ->first();
    $basyo2 = $basyo->keibajyo2moji;
    $keibajyo[] = array('0' => $basyo2, '1' => $kai1, '2' => $basyocode, '3' => $kaisai1);
}

$racekazu = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
])
->orderBy('race')
->get();
foreach($racekazu as $racea){
    $racenum = $racea->race;
    $racetrim = ltrim($racenum, '0');
    if(isset($racea->tokubetu) || isset($racea->mokuyou) || isset($racea->kakutei)){
        $link = '1';
    }else{
        $link = '';
    }
    $raceban[] = array('0' => $racetrim, '1' => $racenum, '2' => $link);
}

$kai0 = ltrim($kai, '0');
$basyo = App\keibajyocd_t::where('keibajyocd', $basyocd)
->first();
$basyo = $basyo->keibajyo2moji;
$kaisai0 = ltrim($kaisai, '0');
$racenum = ltrim($race, '0');
$racemei = App\bangumitest_tbl::where([
    ['kai', '=', $kai],
    ['BASYOCD', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['YRACE', '=', $race],
])
->orderby('negahi', 'desc')
->first();
$win5 = $racemei->win5;
$tokuname = $racemei->BTOKUCDE;
$tokutbl = App\TOKUBETUTEST_TBL::where('BTOKUCD', $tokuname)
->first();
$tokuname = $tokutbl->TOKU6;
$nenjyo = $racemei->SYUBETU;
$jyoken = $racemei->JYOKEN1CD;
$hyojirace['0'] = $win5;
$hyojirace['1'] = $kai0;
$hyojirace['2'] = $basyo;
$hyojirace['3'] = $kaisai0;
$hyojirace['4'] = $racenum;
$hyojirace['5'] = $tokuname;
switch ($nenjyo) {
    case '00':
    $hyojirace['6'] = '3歳';
    break;
    case '02':
    $hyojirace['6'] = '4歳以上';
    break;
    case '15':
    $hyojirace['6'] = '2歳';
    break;
    case '16':
    $hyojirace['6'] = '3歳以上';
    break;
    case '04':
    $hyojirace['6'] = '4歳以上障害';
    break;
    case '18':
    $hyojirace['6'] = '3歳以上障害';
    break;
    default:
    $hyojirace['6'] = '';
    break;
}
switch ($jyoken) {
    case '05':
    $hyojirace['7'] = '500万下';
    break;
    case '10':
    $hyojirace['7'] = '1000万下';
    break;
    case '16':
    $hyojirace['7'] = '1600万下';
    break;
    case '50':
    $hyojirace['7'] = 'オープン';
    break;
    case '51':
    $hyojirace['7'] = '新馬';
    break;
    case '53':
    $hyojirace['7'] = '未勝利';
    break;
    default:
    $hyojirace['7'] = '';
    break;
}

$racemei = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$tokubetu = $racemei->tokubetu;
$sotei = $racemei->sotei;
$mokuyou = $racemei->mokuyou;
$kakutei = $racemei->kakutei;
$odds = $racemei->odds;
$cyokyo = $racemei->cyokyo;
$padock = $racemei->padock;
$girigiri = $racemei->girigiri;
$tm = $racemei->tm;
$kakoichiran = $racemei->kakoichiran;
$cpu = $racemei->cpu;
$danwa = $racemei->danwa;
$point = $racemei->point;
$syouin = $racemei->syouin;
$taisen = $racemei->taisen;
$speed = $racemei->speed;
$rotetion = $racemei->rotetion;
$zenkouhan = $racemei->zenkouhan;
$kyojost = $racemei->kyojost;
$memo = $racemei->memo;
$denmalab = $racemei->denmalab;
$nouryoku_pdf = $racemei->nouryoku_pdf;
$nouryoku_html = $racemei->nouryoku_html;
$tokusyu = $racemei->tokusyu;
$seiseki = $racemei->seiseki;

$racejoken = App\kakuteisyutuba1::where([
    ['kai', '=', $kai],
    ['keibajyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$hasou = $racejoken->hasou;
$kyori = $racejoken->kyori;
$sibadaf = $racejoken->sibada;
$migihidaf = $racejoken->migihida;
$utisotof = $racejoken->utisoto;
$stimer = $racejoken->stimer;
$stimeo = $racejoken->stimeo;
$noucut = $racejoken->noucut;
switch($sibadaf){
    case '0':
    $sibada = 'ダ';
    break;
    case '1':
    $sibada = '芝';
    break;
}
switch($migihidaf){
    case '0':
    $migihida = '右';
    break;
    case '1':
    $migihida = '左';
    break;
    case '2':
    $migihida = '直';
    break;
}
switch($utisotof){
    case '1':
    $utisoto = '外';
    break;
    case '2':
    $utisoto = '外→内';
    break;
    case '3':
    $utisoto = 'タスキ';
    break;
    case '4':
    $utisoto = '大障害';
    break;
    case '5':
    $utisoto = '内2周';
    break;
    default:
    $utisoto = '';
    break;
}
$hyojirace['8'] = substr($hasou, 0, 2);
$hyojirace['9'] = substr($hasou, 2, 2);
$racejyoho['1'] = $kyori.'m';
$racejyoho['2'] = '('.$sibada.'・'.$migihida.$utisoto.')';
$racecut = $noucut;

if ($seiseki == '2'){
    $racebaba = App\msei_tbl::where([
        ['kai', '=', $kai],
        ['basyocd', '=', $basyocd],
        ['kaisai', '=', $kaisai],
        ['race', '=', $race],
    ])
    ->first();
}else{
    $racebaba = App\nowbaba::where('keibajyocd', $basyocd)
    ->first();
}
$tenki = $racebaba->tenki ?? '';
$baba = $racebaba->baba ?? '';
switch($tenki){
    case '0':
    $racebaba['1'] = '晴';
    break;
    case '1':
    $racebaba['1'] = '曇';
    break;
    case '2':
    $racebaba['1'] = '雨';
    break;
    case '3':
    $racebaba['1'] = '小雨';
    break;
    case '4':
    $racebaba['1'] = '雪';
    break;
    case '5':
    $racebaba['1'] = '小雪';
    break;
    case '6':
    $racebaba['1'] = '風';
    break;
    default:
    $racebaba['1'] = '';
    break;
}
switch($baba){
    case '0':
    $racebaba['2'] = '良';
    break;
    case '1':
    $racebaba['2'] = '稍';
    break;
    case '2':
    $racebaba['2'] = '重';
    break;
    case '3':
    $racebaba['2'] = '不';
    break;
    default:
    $racebaba['2'] = '';
    break;
}

$racekane = App\bangumitest_tbl::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['yrace', '=', $race],
])
->orderby('negahi', 'desc')
->first();
$syokin1 = $racekane->syokin1;
$syokin2 = $racekane->syokin2;
$syokin3 = $racekane->syokin3;
$syokin4 = $racekane->syokin4;
$syokin5 = $racekane->syokin5;
$racekane['1'] = substr($syokin1, 0, -4);
$racekane['2'] = substr($syokin2, 0, -4);
$racekane['3'] = substr($syokin3, 0, -4);
$racekane['4'] = substr($syokin4, 0, -4);
$racekane['5'] = substr($syokin5, 0, -4);

$racetime = App\recotime_tbl::where('basyocd', $basyocd)
->where('sibada', $sibadaf)
->where('migihida', $migihidaf)
->where('utisoto', $utisotof)
->where('kyori', $kyori)
->orderby('negahi', 'desc')
->first();
$rectime = $racetime->rectime;
$times = array($rectime, $stimer, $stimeo);
$i = 1;
foreach($times as $time){
    $min = substr($time, 0, 1);
    $sec = substr($time, 1, 2);
    $dem = substr($time, 3, 1);
    $racetime[$i] = $min.'.'.$sec.'.'.$dem;
    $i++;
}

$yoso = App\mfactor::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$i = 1;
$ysnames = array('ysname1', 'ysname2', 'ysname3');
foreach($ysnames as $ysname){
    $raceyoso[] = $yoso->$ysname;
}

$umas = App\kakuteisyutuba2::where([
    ['negahi', '=', $negahi],
    ['keibajyocd', '=', $basyocd],
    ['race', '=', $race],
])
->orderby('umaban')
->get();
foreach ($umas as $uma) {
    $wak = $uma->wakban;
    $ban = $uma->umaban;
    $ban = ltrim($ban, '0');
    $umacd = $uma->umacd;
    $bamei = $uma->kbamei;
    $sex = $uma->seibet;
    switch($sex){
        case '0':
        $seirei = '牡';
        break;
        case '1':
        $seirei = '牝';
        break;
        case '2':
        $seirei = 'セ';
        break;
    }
    $nenrei = $uma->nenrei;
    $nenrei = ltrim($nenrei, '0');
    $seirei = $seirei.$nenrei;
    $bagu = $uma->blink;
    if ($bagu == '1'){
        $bagu = 'B';
    }
    $kinryo = $uma->kinryo;
    $seisu = substr($kinryo, 0, 2);
    $hasu = substr($kinryo, 2);
    if($hasu != '0'){
        $kinryo = $seisu.'.'.$hasu;
    }else{
        $kinryo = $seisu;
    }
    $bataijyu = $uma->bataijyu;
    if($bataijyu != ''){
        $zougen = $uma->zougen;
        $bataijyu = $bataijyu.'('.$zougen.')';
    }
    $genryo = $uma->minarai;
    switch($genryo){
        case '0':
        $genryo = '▲';
        break;
        case '1':
        $genryo = '△';
        break;
        case '2':
        $genryo = '☆';
        break;
    }
    $bkisyu = $uma->bkisyu;
    $nori = $uma->kawarif;
    if ($nori == '1'){
        $nori = '(替)';
    }else{
        $nori = '';
    }
    $kisyu = $nori.$genryo.$bkisyu;
    $trainer = $uma->bkyusya;
    $touzai = $uma->touzai;
    switch($touzai){
        case '1':
        $touzai = '(栗東)';
        break;
        case '2':
        $touzai = '(美浦)';
        break;
        default:
        $touzai = '(他)';
        break;
    }
    $kyusya = $trainer.$touzai;
    $tenkai = $uma->kyaku;
    $kyaku = Tableconvert::nigesen($tenkai);
    $sirusi = App\vfactor::where([
        ['kai', '=', $kai],
        ['basyocd', '=', $basyocd],
        ['kaisai', '=', $kaisai],
        ['race', '=', $race],
    ])
    ->where('umaban', $uma->umaban)
    ->first();
    $sirusicd = $sirusi->ysyoso1;
    $sirusi1 = sirusi($sirusicd);
    $sirusicd = $sirusi->ysyoso2;
    $sirusi2 = sirusi($sirusicd);
    $sirusicd = $sirusi->ysyoso3;
    $sirusi3 = sirusi($sirusicd);
    $sirusicd = $sirusi->fc_yoso;
    $sirusi4 = sirusi($sirusicd);
    $rate = $sirusi->rating;
    $seisu = substr($rate, 0, 2);
    $syosu = substr($rate, 2);
    $rate = $seisu.'.'.$syosu;
    $odds = $uma->odds;
    $odds = sprintf('%.1f', $odds);
    $ninki = $uma->ninki;
    $ninki = $ninki.'人';
    $cyokyo = null;
    $memo = null;
    $cyokyo = App\cyokyo_movie::where([
        ['ykai', '=', $kai],
        ['ybasyocd', '=', $basyocd],
        ['ykaisai', '=', $kaisai],
        ['yrace', '=', $race],
    ])
    ->where('umacd', $umacd)
    ->first();
    if(isset($cyokyo)){
        $cyokyo = '1';
    }
    $memo = App\hpmemo::where('member_no', '123456')
    ->where('umacd', $umacd)
    ->first();
    if(isset($memo)){
        $memo = '1';
    }
    if($seiseki == '2'){
        $cyak = App\vsei_tbl::where([
            ['kai', '=', $kai],
            ['basyocd', '=', $basyocd],
            ['kaisai', '=', $kaisai],
            ['race', '=', $race],
        ])
        ->where('umacd', $umacd)
        ->first();
        $cyak = $cyak->cyakjun;
        $cyak = ltrim($cyak, '0');
        $cyak = $cyak.'着';
    }else {
        $cyak = null;
    }
    $umalist[] = array('0' => $wak, '1' => $ban, '2' => $sirusi1, '3' => $sirusi2, '4' => $sirusi3, '5' => $sirusi4, '6' => $bagu, '7' => $bamei, '8' => $seirei, '9' => $kinryo, '10' => $kisyu, '11' =>$kyusya, '12' =>$memo, '13' =>$cyokyo, '14' =>$rate, '15' => $kyaku, '16' => $bataijyu, '17' => $odds, '18' => $ninki, '19' => $cyak);
}

$kaimes = App\yosorban::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->orderby('outjun', 'asc')
->get();
foreach($kaimes as $kaime){
    //$renban = '';
    $renban = array();
    $yososya = $kaime->tmname;
    $orihonsu = $kaime->orihonsu;
    $i = 1;
    $umatans = array('umatan01', 'umatan02', 'umatan03', 'umatan04', 'umatan05', 'umatan06', 'umatan07', 'umatan08', 'umatan09', 'umatan10', 'umatan11', 'umatan12');
    foreach($umatans as $umatan){
        $omoteura = $kaime->$umatan;
        if($omoteura != ''){
            $omote = substr($omoteura, 0, 2);
            $ura = substr($omoteura, 2);
            $omote = ltrim($omote, '0');
                        $ura = ltrim($ura, '0');
            if($i > $orihonsu){
                $renban[] = $omote.'→'.$ura;
            }else{
                $renban[] = $omote.'⇔'.$ura;
            }
            $i++;
        }
    }
    $tmkaime[] = array('0' => $yososya, '1' => $renban);
}

$kenkai = App\bunmaster::where([
    ['kai', '=', $kai],
    ['keibajyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['kakuteino', '=', $race],
    ['hgenkoumei', '=', '見解'],
])
->orderby('negahi', 'desc')
->first();
if(isset($kenkai)){
    $kenkai = $kenkai->bunsyo;
}

$tenkai = App\bunmaster::where([
    ['kai', '=', $kai],
    ['keibajyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['kakuteino', '=', $race],
    ['hgenkoumei', '=', '展開'],
])
->orderby('negahi', 'desc')
->first();
if(isset($tenkai)){
    $tenkai = $tenkai->bunsyo;
}

$tobikkiris = App\jump::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
    ['syubetu', '=', 'SJ'],
])
->orderby('negahi', 'desc')
->get();
$tobi = null;
foreach ($tobikkiris as $tobikkiri) {
    $tobi[] = $tobikkiri->honbun;
}
$ouendans = App\jump::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
    ['syubetu', '=', 'JS'],
])
->orderby('negahi', 'desc')
->get();
$ouen = null;
foreach ($ouendans as $ouendan) {
    $ouen[] = $ouendan->honbun;
}
$kaime = App\mfactor::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$renban = array();
$orihonsu = $kaime->ori_rat;
$i = 1;
$umatans = array('rtkaime1', 'rtkaime2', 'rtkaime3', 'rtkaime4', 'rtkaime5', 'rtkaime6', 'rtkaime7', 'rtkaime8', 'rtkaime9');
foreach($umatans as $umatan){
    $omoteura = $kaime->$umatan;
    if($omoteura != ''){
        $omote = substr($omoteura, 0, 2);
        $ura = substr($omoteura, 2);
        $omote = ltrim($omote, '0');
                    $ura = ltrim($ura, '0');
        if($i > $orihonsu){
            $renban[] = $omote.'→'.$ura;
        }else{
            $renban[] = $omote.'⇔'.$ura;
        }
        $i++;
    }
}
$tmkaime[] = array('0' => 'レイティング', '1' => $renban);

function sirusi($sirusicd){
    switch($sirusicd){
        case '0':
        return $sirusi = '◎';
        break;
        case '1':
        return $sirusi = '○';
        break;
        case '2':
        return $sirusi = '▲';
        break;
        case '3':
        return $sirusi = '二重';
        break;
        case '4':
        return $sirusi = '△';
        break;
        default:
        return $sirusi = '';
        break;
    }
}
?>

<div id="container">
    <!-- レースの切り替え -->
    <div id="flex_container_top" class="clearfix">
        <div class="raceindex">
            <ul class="negahi clearfix">
                @foreach($dateonly as $aaa)
                @if($aaa[2] == $negahi)
                <li class="active"><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @else
                <li class=""><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @endif
                @endforeach
            </ul>
            <ul class="keibajyo clearfix">
                @foreach($keibajyo as $basyo2)
                @if($basyo2[2] == $basyocd)
                <li class="active"><a href="/keibabook-www/cyuou_syutuba/{{$nen}}{{$basyo2[1]}}{{$basyo2[2]}}{{$basyo2[3]}}{{$race}}" title=test>{{ $basyo2[0] }}</a></li>
                @else
                <li class=""><a href="/keibabook-www/cyuou_syutuba/{{$nen}}{{$basyo2[1]}}{{$basyo2[2]}}{{$basyo2[3]}}{{$race}}" title=test>{{ $basyo2[0] }}</a></li>
                @endif
                @endforeach
            </ul><!-- /.keibajyo -->
            <ul class="race clearfix">
                @foreach($raceban as $racenum)
                @if($racenum[0] == $race)
                <li class="active"><a href="/keibabook-www/cyuou_syutuba/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$racenum[1]}}" title=test>{{ $racenum[0] }}R</a></li>
                @elseif($racenum[2] == '1')
                            <li class=""><a href="/keibabook-www/cyuou_syutuba/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$racenum[1]}}" title=test>{{ $racenum[0] }}R</a></li>
                @else
                    <li class="">{{ $racenum[0] }}R</a></li>
                @endif
                @endforeach
            </ul><!-- /.race -->
        </div>
        <div class="racename">
            @if($hyojirace[0] != '')
            <p>WIN5対象レース</p>
            @endif
            <p>{{ $hyojirace[1] }}回{{$hyojirace[2]}}{{$hyojirace[3]}}日目{{$hyojirace[4]}}R</p>
            <p>{{ $hyojirace[5] }}</p>
            <p>{{ $hyojirace[6] }}{{$hyojirace[7]}}</p>
            <p>発走{{ $hyojirace[8] }}:{{$hyojirace[9]}}</p>
        </div><!-- /.menuindex -->
        <div class="racejyoken">
            <p>{{ $racejyoho[1] }}</p>
            <p>{{ $racejyoho[2] }}</p>
            @if($racebaba[1] != '')
            <p>{{ $racebaba[1] }}・{{ $racebaba[2] }}</p>
            @endif
        </div><!-- /.menuindex -->
        <div class="racesyokin">
            <p>①{{ $racekane[1] }}万円</p>
            <p>②{{ $racekane[2] }}万円</p>
            <p>③{{ $racekane[3] }}万円</p>
            <p>④{{ $racekane[4] }}万円</p>
            <p>⑤{{ $racekane[5] }}万円</p>
        </div><!-- /.menuindex -->
        <div class="racetime">
            @if(empty($seiseki))
            <p>レコード{{ $racetime[1] }}</p>
            @endif
            <p>推定タイム</p>
            <p>良{{ $racetime[2] }}</p>
            <p>重{{ $racetime[3] }}</p>
        </div><!-- /.menuindex -->
        <div class="racecut">
            <p>{{ $racecut }}</p>
        </div><!-- /.menuindex -->
    </div><!-- /#flex_container_top -->
</div>

ここにお待たせ
<div class="menuindex">
    <ul class="cyuou clearfix">
        @if (isset($tokubetu))
        <li class=""><a href="/keibabook-www/cyuou_tokubetu/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="特別登録馬">特別登録馬</a></li>
        @else
        <li class="">特別登録馬</li>
        @endif
        @if (isset($sotei))
        <li class=""><a href="/keibabook-www/cyuou_sotei/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="想定馬">想定馬</a></li>
        @else
        <li class="">想定馬</li>
        @endif
        @if (isset($mokuyou))
        <li class=""><a href="/keibabook-www/cyuou_thursday/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="枠順確定前出馬">枠順確定前出馬</a></li>
        @else
        <li class="">枠順確定前出馬</li>
        @endif
        @if (isset($kakutei))
        <li class="active"><a href="/keibabook-www/cyuou_syutuba/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="確定出馬">確定出馬</a></li>
        @else
        <li class="">確定出馬</li>
        @endif
    </ul>
</div><!-- /.menuindex -->

<!-- ここからコンテンツ -->
<div id="flex_container" class="clearfix">
    <div id="fullcontent">
        <div class="section">
            <table class="syutuba width100" id="syutuba_sort_table">
                <thead>
                    <tr>
                        <th>枠<br>番</th>
                        <th>馬<br>番</th>
                        <th>{{$raceyoso[0]}}</th>
                        <th>{{$raceyoso[1]}}</th>
                        <th>{{$raceyoso[2]}}</th>
                        <th>CPU</th>
                        <th>馬<br>具</th>
                        <th>馬名</th>
                        <th>性齢</th>
                        <th>斤<br>量</th>
                        <th>騎手</th>
                        <th>厩舎</th>
                        <th>メモ</th>
                        <th>調教</th>
                        <th>rate</th>
                        <th>脚質</th>
                        <th>馬体重</th>
                        <th>単勝</th>
                        <th>人気</th>
                        <th>着順</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($umalist as $uma)
                    <tr>
                        @for($i = 0; $i < 20; $i++)
                        <th>{{$uma[$i]}}</th>
                        @endfor
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- 見解,展開,推理のキー,トラックマン予想連番 -->
        <div class="flex_syutuba clearfix">
            <!-- 左側 -->
            <div class="flex_syutuba_left">
                <!-- 見解 -->
                <div class="boxsection">
                    <p class="title">見解</p>
                    <p>{{$kenkai}}</p>
                </div><!-- /.boxsection -->
                <!-- 展開 -->
                <div class="boxsection">
                    <p class="title">展開</p>
                    <p>{{$tenkai}}</p>
                </div><!-- /.boxsection -->
                <!-- 推理のキー -->
                <div class="boxsection">
                    <p class="title">推理のキー</p>
                    <p>ここに推理のキー</p>
                </div><!-- /.boxsection -->
            </div><!-- /.flex_syutuba_left -->

            <!-- 右側 -->
            <div class="flex_syutuba_right">
                <!-- トラックマン予想連番 -->
                <div class="boxsection">
                    <p class="title">トラックマン予想連番</p>
                    <div class="renban">
                        <table class="renban width100">
                            <tbody>
                                @foreach($tmkaime as $kaime)
                                <tr>
                                    <td class="name">{{$kaime[0]}}</td>
                                    @foreach($kaime[1] as $renban)
                                    <td>{{$renban}}</td>
                                    @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.boxsection -->
            </div><!-- /.flex_syutuba_left -->
        </div><!-- /.flex_syutuba -->

        <!-- 追い切りピカイチ,厩舎回り特選,トビっきりこぼれ話,トジャンプ競走応援団 -->
        <!-- タブ切り替え -->
        <div class="section" style="コラム">
            <!-- タブメニュー -->
            <ul id="tab-menu">
                <li class="active" style="おいきり">追い切りピカイチ</li>
                <li style="厩舎">厩舎回り特選</li>
                @if(isset($tobi))
                <li style="トビ">トビっきりこぼれ話</li>
                @endif
                @if(isset($ouen))
                <li style="ジャンプ">ジャンプ競走応援団</li>
                @endif
            </ul><!-- /#tab-menu -->
            <!-- タブの中身 -->
            <div id="tab-box">
                <div class="active" style="おいきり">追い切りピカイチ</div>
                <div style="厩舎">厩舎回り特選</div>
                @if(isset($ouen))
                @foreach($tobi as $tobis)
                <div style="トビ">{{$tobis}}</div>
                @endforeach
                @endif
                @if(isset($ouen))
                @foreach($ouen as $ouens)
                <div style="ジャンプ">{{$ouens}}</div>
                @endforeach
                @endif
            </div><!-- /#tab-box -->
        </div><!-- /.section -->
    </div>
</div>
        @endsection
