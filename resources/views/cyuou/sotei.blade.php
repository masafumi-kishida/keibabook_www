
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
出馬
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
<link href="/keibabook-www/css/reset.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/clear.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/global.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
{{-- ホームactive --}}
{{ $globalmenu_cyuou = "active" }}
{{ $globalnavi_nittei = "active" }}
@include('common.header')
@endsection

@section('content')
<!-- お知らせ -->
<div class="oshirase clearfix">

    <ul class="">
        <li class="midashi">速報</li>
        <li class="title"><a href="/">5/21（日）東京10Rのレース結果が確定しました。</a></li>
    </ul><!-- /. -->

    <p class="">

    </p>

</div><!-- /.oshirase -->

<?php

//カーボン
use Carbon\Carbon;
use App\Facades\Tableconvert;

$date = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$negahi = $date->negahi;
$entryday = App\kaisaidata1_tbl::where([
    ['negahi', '=', $negahi],
    ['basyocd', '=', $basyocd],
])
->first();
$syume = $entryday->syume;
$week_str_list = array( '日', '月', '火', '水', '木', '金', '土');
$entryday = App\kaisaidata1_tbl::where('syume', $syume)
->orderBy('negahi')
->get();
$datej = '0';
foreach($entryday as $entry){
    $date = Carbon::parse($entry->negahi);
    if ($date == $datej){
        continue;
    }
    $datej = $date;
    $monday = $date->format('n/j');
    $week =  $date->dayOfWeek;
    $weeklist = $week_str_list[$week];
    $ymd = $date->format('Ymd');
    $dateonly[] = array('0' => $monday, '1' => $weeklist, '2' => $ymd);
}
$entrybasyo = App\kaisaidata1_tbl::where('negahi', $negahi)
->where('welflg', '1')
->get();
foreach($entrybasyo as $entry){
    $basyocode = $entry->basyocd;
    $basyo = App\keibajyocd_t::where('keibajyocd', $basyocode)
    ->first();
    $basyo2 = $basyo->keibajyo2moji;
    $keibajyo[] = array('0' => $basyo2, '1' => $basyocode);
}
$entrybasyo = App\kaisaidata1_tbl::where('negahi', $negahi)
->wherein('welflg', ['0', '2'])
->orderby('welflg')
->get();
foreach($entrybasyo as $entry){
    $kai1 = $entry->kai;
    $basyocode = $entry->basyocd;
    $kaisai1 = $entry->kaisai;
    $basyo = App\keibajyocd_t::where('keibajyocd', $basyocode)
    ->first();
    $basyo2 = $basyo->keibajyo2moji;
    $keibajyo[] = array('0' => $basyo2, '1' => $kai1, '2' => $basyocode, '3' => $kaisai1);
}

$racekazu = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
])
->orderBy('race')
->get();
foreach($racekazu as $racea){
    $racenum = $racea->race;
    $racetrim = ltrim($racenum, '0');
    if(isset($racea->tokubetu) || isset($racea->mokuyou) || isset($racea->kakutei)){
        $link = '1';
    }else{
        $link = '';
    }
    $raceban[] = array('0' => $racetrim, '1' => $racenum, '2' => $link);
}

$racemei = App\bangumitest_tbl::where([
    ['kai', '=', $kai],
    ['BASYOCD', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['YRACE', '=', $race],
])
->orderby('negahi', 'desc')
->first();
$kai0 = ltrim($kai, '0');
$basyo = App\keibajyocd_t::where('keibajyocd', $basyocd)
->first();
$basyo = $basyo->keibajyo2moji;
$kaisai0 = ltrim($kaisai, '0');
$racenum = ltrim($race, '0');
$win5 = $racemei->win5;
$hyojirace['0'] = $win5;
$hyojirace['1'] = $kai0;
$hyojirace['2'] = $basyo;
$hyojirace['3'] = $kaisai0;
$hyojirace['4'] = $racenum;

$tokuname = $racemei->BTOKUCDE;
$tokutbl = App\TOKUBETUTEST_TBL::where('BTOKUCD', $tokuname)
->first();
$tokuname = $tokutbl->TOKU6;
$nenjyo = $racemei->SYUBETU;
$jyoken = $racemei->JYOKEN1CD;
$hyojirace['5'] = $tokuname;
switch ($nenjyo) {
    case '00':
    $hyojirace['6'] = '3歳';
    break;
    case '02':
    $hyojirace['6'] = '4歳以上';
    break;
    case '15':
    $hyojirace['6'] = '2歳';
    break;
    case '16':
    $hyojirace['6'] = '3歳以上';
    break;
    case '04':
    $hyojirace['6'] = '4歳以上障害';
    break;
    case '18':
    $hyojirace['6'] = '3歳以上障害';
    break;
    default:
    $hyojirace['6'] = '';
    break;
}
switch ($jyoken) {
    case '05':
    $hyojirace['7'] = '500万下';
    break;
    case '10':
    $hyojirace['7'] = '1000万下';
    break;
    case '16':
    $hyojirace['7'] = '1600万下';
    break;
    case '50':
    $hyojirace['7'] = 'オープン';
    break;
    case '51':
    $hyojirace['7'] = '新馬';
    break;
    case '53':
    $hyojirace['7'] = '未勝利';
    break;
    default:
    $hyojirace['7'] = '';
    break;
}

$racemei = App\kaisaidata2_tbl::where([
    ['nen', '=', $nen],
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$tokubetu = $racemei->tokubetu;
$sotei = $racemei->sotei;
$mokuyou = $racemei->mokuyou;
$kakutei = $racemei->kakutei;
$odds = $racemei->odds;
$cyokyo = $racemei->cyokyo;
$padock = $racemei->padock;
$girigiri = $racemei->girigiri;
$tm = $racemei->tm;
$kakoichiran = $racemei->kakoichiran;
$cpu = $racemei->cpu;
$danwa = $racemei->danwa;
$point = $racemei->point;
$syouin = $racemei->syouin;
$taisen = $racemei->taisen;
$speed = $racemei->speed;
$rotetion = $racemei->rotetion;
$zenkouhan = $racemei->zenkouhan;
$kyojost = $racemei->kyojost;
$memo = $racemei->memo;
$denmalab = $racemei->denmalab;
$nouryoku_pdf = $racemei->nouryoku_pdf;
$nouryoku_html = $racemei->nouryoku_html;
$tokusyu = $racemei->tokusyu;
$seiseki = $racemei->seiseki;

$racejoken = App\kakuteisyutuba1::where([
    ['kai', '=', $kai],
    ['keibajyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['race', '=', $race],
])
->first();
$hasou = $racejoken->hasou;
$hyojirace['8'] = substr($hasou, 0, 2);
$hyojirace['9'] = substr($hasou, 2, 2);
$kyori = $racejoken->kyori;
$sibadaf = $racejoken->sibada;
$migihidaf = $racejoken->migihida;
$utisotof = $racejoken->utisoto;
$stimer = $racejoken->stimer;
$stimeo = $racejoken->stimeo;
$noucut = $racejoken->noucut;
switch($sibadaf){
    case '0':
    $sibada = 'ダ';
    break;
    case '1':
    $sibada = '芝';
    break;
}
switch($migihidaf){
    case '0':
    $migihida = '右';
    break;
    case '1':
    $migihida = '左';
    break;
    case '2':
    $migihida = '直';
    break;
}
switch($utisotof){
    case '1':
    $utisoto = '外';
    break;
    case '2':
    $utisoto = '外→内';
    break;
    case '3':
    $utisoto = 'タスキ';
    break;
    case '4':
    $utisoto = '大障害';
    break;
    case '5':
    $utisoto = '内2周';
    break;
    default:
    $utisoto = '';
    break;
}
$racejyoho['1'] = $kyori.'m';
$racejyoho['2'] = '('.$sibada.'・'.$migihida.$utisoto.')';
$racecut = $noucut;


$racekane = App\bangumitest_tbl::where([
    ['kai', '=', $kai],
    ['basyocd', '=', $basyocd],
    ['kaisai', '=', $kaisai],
    ['yrace', '=', $race],
])
->orderby('negahi', 'desc')
->first();
$syokin1 = $racekane->syokin1;
$syokin2 = $racekane->syokin2;
$syokin3 = $racekane->syokin3;
$syokin4 = $racekane->syokin4;
$syokin5 = $racekane->syokin5;
$racekane['1'] = substr($syokin1, 0, -4);
$racekane['2'] = substr($syokin2, 0, -4);
$racekane['3'] = substr($syokin3, 0, -4);
$racekane['4'] = substr($syokin4, 0, -4);
$racekane['5'] = substr($syokin5, 0, -4);

$umas = App\tmsotei::where([
    ['kaisai', '=', $kaisai],
    ['keibajyocd', '=', $basyocd],
    ['yoteiraceno', '=', $race],
])
->orderby('kbamei')
->get();
foreach ($umas as $uma) {
    $umacd = $uma->umacd;
    $bamei = $uma->kbamei;
    $sex = $uma->seibet;
    switch($sex){
        case '0':
        $seirei = '牡';
        break;
        case '1':
        $seirei = '牝';
        break;
        case '2':
        $seirei = 'セ';
        break;
    }
    $nenrei = $uma->nenrei;
    $seirei = $seirei.$nenrei;
    $kinryo = $uma->kinryo;
    $seisu = substr($kinryo, 0, 2);
    $hasu = substr($kinryo, 2);
    if($hasu != '0'){
        $kinryo = $seisu.'.'.$hasu;
    }else{
        $kinryo = $seisu;
    }
    $genryo = $uma->minarai;
    switch($genryo){
        case '0':
        $genryo = '▲';
        break;
        case '1':
        $genryo = '△';
        break;
        case '2':
        $genryo = '☆';
        break;
    }
    $kisyu = $uma->bkisyu;
    $kyusya = $uma->bkyusya;
    $cyokyo = null;
    $cyokyo = App\cyokyo_movie::where([
        ['ykai', '=', $kai],
        ['ybasyocd', '=', $basyocd],
        ['ykaisai', '=', $kaisai],
        ['yrace', '=', $race],
    ])
    ->where('umacd', $umacd)
    ->first();
    if(isset($cyokyo)){
        $cyokyo = '1';
    }
    $zekken = App\zekken_master::where('umacd', $umacd)
    ->wherein('basyo', ['0', '1'])
    ->first();
    $zekkennum = $zekken->zekken;
    $umalist[] = array('0' => $zekkennum ,'1' => $bamei, '2' => $seirei, '3' => $kinryo, '4' => $kisyu, '5' =>$kyusya, '6' =>$cyokyo);
}

function sirusi($sirusicd){
    switch($sirusicd){
        case '0':
        return $sirusi = '◎';
        break;
        case '1':
        return $sirusi = '○';
        break;
        case '2':
        return $sirusi = '▲';
        break;
        case '3':
        return $sirusi = '二重';
        break;
        case '4':
        return $sirusi = '△';
        break;
        default:
        return $sirusi = '';
        break;
    }
}
?>

<div id="container">
    <!-- レースの切り替え -->
    <div id="flex_container_top" class="clearfix">
        <div class="raceindex">
            <ul class="negahi clearfix">
                @foreach($dateonly as $aaa)
                @if($aaa[2] == $negahi)
                <li class="active"><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @else
                <li class=""><a href="/" title=test>{{ $aaa[0] }}({{ $aaa[1] }})</a></li>
                @endif
                @endforeach
            </ul>

            <ul class="keibajyo clearfix">
                @foreach($keibajyo as $basyo2)
                @if($basyo2[2] == $basyocd)
                <li class="active"><a href="http://localhost/keibabook-laravel/cyuou_syutuba/{{$nen}}{{$basyo2[1]}}{{$basyo2[2]}}{{$basyo2[3]}}{{$race}}" title=test>{{ $basyo2[0] }}</a></li>
                @else
                <li class=""><a href="http://localhost/keibabook-laravel/cyuou_syutuba/{{$nen}}{{$basyo2[1]}}{{$basyo2[2]}}{{$basyo2[3]}}{{$race}}" title=test>{{ $basyo2[0] }}</a></li>
                @endif
                @endforeach
            </ul><!-- /.keibajyo -->
            <ul class="race clearfix">
                @foreach($raceban as $racenum)
                @if($racenum[1] == $race)
                <li class="active"><a href="http://localhost/keibabook-laravel/cyuou_sotei/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$racenum[1]}}" title=test>{{ $racenum[0] }}R</a></li>
                @elseif($racenum[2] == '1')
                <li class=""><a href="http://localhost/keibabook-laravel/cyuou_syutuba/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$racenum[1]}}" title=test>{{ $racenum[0] }}R</a></li>
                @else
                                <li class="">{{ $racenum[0] }}R</a></li>
                                @endif
                @endforeach
            </ul><!-- /.race -->
        </div>
        <div class="racename">
            @if($hyojirace[0] != '')
            <p>WIN5対象レース</p>
            @endif
            <p>{{ $hyojirace[1] }}回{{$hyojirace[2]}}{{$hyojirace[3]}}日目{{$hyojirace[4]}}R</p>
            <p>{{ $hyojirace[5] }}</p>
            <p>{{ $hyojirace[6] }}{{$hyojirace[7]}}</p>
            <p>発走{{ $hyojirace[8] }}:{{$hyojirace[9]}}</p>
        </div><!-- /.menuindex -->
        <div class="racejyoken">
            <p>{{ $racejyoho[1] }}</p>
            <p>{{ $racejyoho[2] }}</p>
        </div><!-- /.menuindex -->
        <div class="racesyokin">
            <p>①{{ $racekane[1] }}万円</p>
            <p>②{{ $racekane[2] }}万円</p>
            <p>③{{ $racekane[3] }}万円</p>
            <p>④{{ $racekane[4] }}万円</p>
            <p>⑤{{ $racekane[5] }}万円</p>
        </div><!-- /.menuindex -->
    </div><!-- /#flex_container_top -->
</div>


ここにお待たせ

<div class="menuindex">
    <ul class="cyuou clearfix">
        @if (isset($tokubetu))
        <li class=""><a href="http://localhost/keibabook-laravel/cyuou_tokubetu/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="特別登録馬">特別登録馬</a></li>
        @else
        <li class="">特別登録馬</li>
        @endif
        @if (isset($sotei))
        <li class="active"><a href="http://localhost/keibabook-laravel/cyuou_sotei/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="想定馬">想定馬</a></li>
        @else
        <li class="">想定馬</li>
        @endif
        @if (isset($mokuyou))
        <li class=""><a href="http://localhost/keibabook-laravel/cyuou_thursday/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="枠順確定前出馬">枠順確定前出馬</a></li>
        @else
        <li class="">枠順確定前出馬</li>
        @endif
        @if (isset($kakutei))
        <li class=""><a href="http://localhost/keibabook-laravel/cyuou_syutuba/{{$nen}}{{$kai}}{{$basyocd}}{{$kaisai}}{{$race}}" title="確定出馬">確定出馬</a></li>
        @else
        <li class="">確定出馬</li>
        @endif
    </ul>
</div><!-- /.menuindex -->

<!-- ここからコンテンツ -->
<div id="flex_container" class="clearfix">

    <div id="fullcontent">

        <!-- コンテンツの切り替え -->
        <!-- ここは可変で出力して下さい -->

        <div class="section">

            <table class="syutuba width100" id="syutuba_sort_table">
                <thead>
                    <tr>
                        <th>ゼッケン番号</th>
                        <th>馬名</th>
                        <th>性齢</th>
                        <th>斤<br>量</th>
                        <th>騎手</th>
                        <th>厩舎</th>
                        <th>調教</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($umalist as $uma)
                    <tr>
                        @for($i = 0; $i < 7; $i++)
                        <th>{{$uma[$i]}}</th>
                        @endfor
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        @endsection
