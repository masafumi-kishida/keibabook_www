<div class="menuindex">
    <ul class="cyuou clearfix">
        @if (isset($tokubetu))
        <li class="active"><a href="/cyuou-syutuba/{{ $race-nagahi }}" title="出馬表">出馬表</a></li>
        @else
        <li class="">出馬表</li>
        @endif
        @if (isset($odds))
        <li class=""><a href="/cyuou-odds/{{ $race-negahi }}" title="オッズ">オッズ</a></li>
        @else
        <li class="">オッズ</li>
        @endif
        @if (isset($cyokyo))
        <li class=""><a href="/testsite/www/cyuou/cyokyo.php" title="調教">調教</a></li>
        @else
        <li class="">調教</li>
        @endif
        @if (isset($paddok))
        <li class=""><a href="/testsite/www/cyuou/paddok.php" title="パドック情報">パドック情報</a></li>
        @else
        <li class="">パドック情報</li>
        @endif
        @if (isset($girigiri))
        <li class=""><a href="/testsite/www/cyuou/girigiri.php" title="ギリギリ情報">ギリギリ情報</a></li>
        @else
        <li class="">ギリギリ情報</li>
        @endif
        @if (isset($template))
        <li class=""><a href="/testsite/www/cyuou/cyokuzen.php" title="トラックマン直前情報">TM直前情報</a></li>
        @else
        <li class="">TM直前情報</li>
        @endif
        @if (isset($kakoichiran))
        <li class=""><a href="/testsite/www/cyuou/kako.php" title="過去一覧">過去一覧</a></li>
        @else
        <li class="">過去一覧</li>
        @endif
        @if (isset($cpu))
        <li class=""><a href="/testsite/www/cyuou/cpu.php" title="コンピュータ予想">コンピュータ予想</a></li>
        @else
        <li class="">コンピュータ予想</li>
        @endif
        @if (isset($danwa))
        <li class=""><a href="/testsite/www/cyuou/danwa.php" title="厩舎の話">厩舎の話</a></li>
        @else
        <li class="">厩舎の話</li>
        @endif
        @if (isset($syouin))
        <li class=""><a href="/testsite/www/cyuou/syoin.php" title="前走の勝因敗因">前走の勝因敗因</a></li>
        @else
        <li class="">前走の勝因敗因</li>
        @endif
        @if (isset($taisen))
        <li class=""><a href="/testsite/www/cyuou/taisen.php" title="対戦成績">対戦成績</a></li>
        @else
        <li class="">対戦成績</li>
        @endif
        @if (isset($speed))
        <li class=""><a href="/testsite/www/cyuou/speed.php" title="スピード指数">スピード指数</a></li>
        @else
        <li class="">スピード指数</li>
        @endif
        @if (isset($rotetion))
        <li class=""><a href="/testsite/www/cyuou/rote.php" title="ローテーション">ローテーション</a></li>
        @else
        <li class="">ローテーション</li>
        @endif
        @if (isset($zenkouhan))
        <li class=""><a href="/testsite/www/cyuou/zenkouhan.php" title="前後半タイム">前後半タイム</a></li>
        @else
        <li class="">前後半タイム</li>
        @endif
        @if (isset($kyojost))
        <li class=""><a href="/testsite/www/cyuou/kyojost.php" title="今日の騎手厩舎">今日の騎手厩舎</a></li>
        @else
        <li class="">今日の騎手厩舎</li>
        @endif
        @if (isset($memo))
        <li class=""><a href="/testsite/www/cyuou/memo.php" title="Ｍｙメモ">Ｍｙメモ</a></li>
        @else
        <li class="">Ｍｙメモ</li>
        @endif
        @if (isset($denmalab))
        <li class=""><a href="/testsite/www/cyuou/denmalab.php" title="究極分析">究極分析</a></li>
        @else
        <li class="">究極分析</li>
        @endif
        @if (isset($nouryoku_pdf))
        <li class=""><a href="/testsite/www/cyuou/nrpaperpdf.php" title="能力表（PDF）">能力表（PDF）</a></li>
        @else
        <li class="">能力表（PDF）</li>
        @endif
        @if (isset($nouryoku_html))
        <li class=""><a href="/testsite/www/cyuou/nrpaperhtml.php" title="能力表（Html）">能力表（Html）</a></li>
        @else
        <li class="">能力表（Html）</li>
        @endif
        @if (isset($photopaddoc))
        <li class=""><a href="/testsite/www/cyuou/photopaddok.php" title="PHOTOパドック">PHOTOパドック</a></li>
        @else
        <li class="">&nbsp;</li>
        @endif
        @if (isset($tokusyu))
        <li class=""><a href="/testsite/www/cyuou/tokusyu.php" title="{{$hyojirace[5]}}特集へ">{{$hyojirace[5]}}特集へ</a></li>
        @else
        <li class="">&nbsp;</li>
        @endif
        <li class="">&nbsp;</li>
        <li class="">&nbsp;</li>
        @if (isset($seiseki))
        <li class=""><a href="/testsite/www/cyuou/seiseki.php" title="レース結果">レース結果</a></li>
        @else
        <li class="">レース結果</li>
        @endif
    </ul>
</div><!-- /.menuindex -->


<!-- 特別登録馬・想定馬・枠順確定前出馬・確定出馬の切り替え -->
<!-- ここは可変で出力して下さい -->
<div class="menuindex">
    <ul class="cyuou clearfix">
        @if (isset($tokubetu))
        <li class=""><a href="/testsite/www/cyuou/tokubetu.php" title="特別登録馬">特別登録馬</a></li>
        @else
        <li class="">特別登録馬</li>
        @endif
        @if (isset($sotei))
        <li class=""><a href="/testsite/www/cyuou/sotei.php" title="想定馬">想定馬</a></li>
        @else
        <li class="">想定馬</li>
        @endif
        @if (isset($mokuyou))
        <li class=""><a href="/testsite/www/cyuou/wakujunkakuteimae.php" title="枠順確定前出馬">枠順確定前出馬</a></li>
        @else
        <li class="">枠順確定前出馬</li>
        @endif
        @if (isset($kakutei))
        <li class="active"><a href="/testsite/www/cyuou/syutuba.php" title="確定出馬">確定出馬</a></li>
        @else
        <li class="">確定出馬</li>
        @endif
    </ul>
</div><!-- /.menuindex -->
