<!-- コンテンツの切り替え -->
{{--
  $nen='yyyy'      該当のyyyy=西暦
  $kai='kk'        該当のkk=回
  $basyocd='bb'    該当のbb=競馬場コード
  $kaisai='hh'     該当のhh=開催日
  $race='rr'       該当のrr=レース番号
--}}




<div class="menuindex">
  <ul class="cyuou clearfix">

<?php

  if (isset($nen)
      && isset($kai)
      && isset($basyocd)
      && isset($kaisai)
      && isset($race)) {
    // 開催データ２リード
    $negahi = $nen . $kai . $basyocd . $kaisai . $race;
    $racedatas = App\KAISAIDATA2_TBL::where('nen', $nen)
                            ->where('kai', $kai)
                            ->where('basyocd', $basyocd)
                            ->where('kaisai', $kaisai)
                            ->where('race', $race)
                            ->get();
    if ($racedatas->count() == 1) {
      //$racedata = $racedatas->first();
      $racedata = $racedatas[0];
      // １件有った
      if ($racedata->kakutei == '1') {
        // 確定出馬link
        echo "<li class=\"active\"><a href=\"/cyuou_syutuba/$negahi\" title=\"出馬表\">出馬表</a></li>";
      } else {
        echo '<li class="">出馬表</li>';
      }
      if ($racedata->odds == '1') {
        // オッズlink
        echo "<li class=\"active\"><a href=\"/cyuou_odds/$negahi\" title=\"オッズ\">オッズ</a></li>";
      } else {
        echo '<li class="">オッズ</li>';
      }
      if ($racedata->cyokyo == '1') {
        // 調教link
        echo "<li class=\"active\"><a href=\"/cyuou_cyokyo/$negahi\" title=\"調教\">調教</a></li>";
      } else {
        echo '<li class="">調教</li>';
      }
      if ($racedata->paddok == '1') {
        // パドック情報link
        echo "<li class=\"active\"><a href=\"/cyuou_paddok/$negahi\" title=\"パドック情報\">パドック情報</a></li>";
      } else {
        echo '<li class="">パドック情報</li>';
      }
      if ($racedata->girigiri == '1') {
        // ギリギリ情報link
        echo "<li class=\"active\"><a href=\"/cyuou_girigiri/$negahi\" title=\"ギリギリ情報\">ギリギリ情報</a></li>";
      } else {
        echo '<li class="">ギリギリ情報</li>';
      }
      if ($racedata->tm == '1') {
        // トラックマン直前情報link
        echo "<li class=\"active\"><a href=\"/cyuou_cyokuzen/$negahi\" title=\"トラックマン直前情報\">ＴＭ直前情報</a></li>";
      } else {
        echo '<li class="">ＴＭ直前情報</li>';
      }
      if ($racedata->kakoichiran == '1') {
        // 過去一覧link
        echo "<li class=\"active\"><a href=\"/cyuou_kako/$nagahi\" title=\"過去一覧\">過去一覧</a></li>";
      } else {
        echo '<li class="">過去一覧</li>';
      }
      if ($racedata->cpu == '1') {
        // コンピュータ予想link
        echo "<li class=\"active\"><a href=\"/cyuou_cpu/$negahi\" title=\"コンピュータ予想\">コンピュータ予想</a></li>";
      } else {
        echo '<li class="">コンピュータ予想</li>';
      }
      if ($racedata->danwa == '1') {
        // 厩舎のはなしlink
        echo "<li class=\"active\"><a href=\"/cyuou_danwa/$negahi\" title=\"厩舎の話\">厩舎の話</a></li>";
      } else {
        echo '<li class="">厩舎の話</li>';
      }
      if ($racedata->syoin == '1') {
        // 前走の勝因敗因link
        echo "<li class=\"active\"><a href=\"/cyuou_syoin/$negahi\" title=\"前走の勝因敗因\">前走の勝因敗因</a></li>";
      } else {
        echo '<li class="">前走の勝因敗因</li>';
      }
      if ($racedata->taisen == '1') {
        // 対戦成績link
        echo "<li class=\"active\"><a href=\"/cyuou_taisen/$negahi\" title=\"対戦成績\">対戦成績</a></li>";
      } else {
        echo '<li class="">対戦成績</li>';
      }
      if ($racedata->speed == '1') {
        // スピード指数link
        echo "<li class=\"active\"><a href=\"/cyuou_speed/$negahi\" title=\"スピード指数\">スピード指数</a></li>";
      } else {
        echo '<li class="">スピード指数</li>';
      }
      if ($racedata->rotetion == '1') {
        // ローテーションlink
        echo "<li class=\"active\"><a href=\"/cyuou_rote/$negahi\" title=\"ローテーション\">ローテーション</a></li>";
      } else {
        echo '<li class="">ローテーション</li>';
      }
      if ($racedata->zenkouhan == '1') {
        // 前後半タイムlink
        echo "<li class=\"active\"><a href=\"/cyuou_zenkouhan/$negahi\" title=\"前後半タイム\">前後半タイム</a></li>";
      } else {
        echo '<li class="">前後半タイム</li>';
      }
      if ($racedata->kyojost == '1') {
        // 今日の騎手厩舎link
        echo "<li class=\"active\"><a href=\"/cyuou_kyojost/$negahi\" title=\"今日の騎手厩舎\">今日の騎手厩舎</a></li>";
      } else {
        echo '<li class="">今日の騎手厩舎</li>';
      }
      if ($racedata->memo == '1') {
        // Myメモlink
        echo "<li class=\"active\"><a href=\"/cyuou_memo/$negahi\" title=\"Ｍｙメモ\">Ｍｙメモ</a></li>";
      } else {
        echo '<li class="">Ｍｙメモ</li>';
      }
      if ($racedata->denmalab == '1') {
        // 究極分析link
        echo "<li class=\"active\"><a href=\"/cyuou_denmalab/$negahi\" title=\"究極分析\">究極分析</a></li>";
      } else {
        echo '<li class="">究極分析</li>';
      }
      if ($racedata->nouryoku_pdf == '1') {
        // 能力表pdflink
        echo "<li class=\"active\"><a href=\"/cyuou_nrpaperpdf/$negahi\" title=\"能力表（PDF)\">能力表(PDF)</a></li>";
      } else {
        echo '<li class="">能力表(PDF)</li>';
      }
      if ($racedata->nouryoku_html == '1') {
        // 能力表htmllink
        echo "<li class=\"active\"><a href=\"/cyuou_nrpaperhtml/$negahi\" title=\"能力表(Html)\">能力表(Html)</a></li>";
      } else {
        echo '<li class="">能力表(Html)</a></li>';
      }
      if ($racedata->photopaddok == '1') {
        // photoパドックlink
        echo "<li class=\"active\"><a href=\"/cyuou_photopaddok/$negahi\" title=\"PHOTOパドック\">PHOTOパドック</a></li>";
      } else {
        echo '<li class="">PHOTOパドック</li>';
      }
      if ($racedata->tokusyu == '1') {
        // ○○○○特集link
        echo "<li class=\"active\"><a href=\"/cyuou_tokusyu/$negahi\" title=\"○○○○特集へ\">○○○○特集へ</a></li>";
      } else {
        echo '<li class="">○○○○特集へ</li>';
      }
      if ($racedata->seiseki == '1' || $racedata->seiseki == '2') {
        // レース結果link
        echo "<li class=\"active\"><a href=\"/cyuou_seiseki/$negahi\" title=\"レース結果\">レース結果</a></li>";
      } else {
        echo '<li class="">レース結果</li>';
      }
    }
  }

?>

  </ul>
</div>
