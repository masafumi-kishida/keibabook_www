
{{--
  <<ネット新聞ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_netpaper="active"      ネット新聞をactiveにするとき
  $globalnavi_noryoku="active"       能力表をactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_netpaper) ? : $globalnavi_netpaper = "" @endphp
@php isset($globalnavi_noryoku) ? : $globalnavi_noryoku = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_netpaper }}"><a href="/" title="ネット新聞">ネット新聞</a></li>
      <li class="{{ $globalnavi_noryoku }}"><a href="/" title="能力表">能力表</a></li>
    </ul>
  </div><!-- /.globalnavi -->
