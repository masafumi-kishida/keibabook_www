{{--
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalmenu_home="active"      HOMEをactiveにするとき
  $globalmenu_cyuuou="active"    中央競馬をactiveにするとき
  $globalmenu_chihou="active"    地方競馬をactiveにするとき
  $globalmenu_kaigai="active"    海外競馬をactiveにするとき
  $globalmenu_news="active"      ニュースをactiveにするとき
  $globalmenu_db="active"        データ検索をactiveにするとき
  $globalmenu_column="active"    コラムをactiveにするとき
  $globalmenu_netpaper="active"  ネット新聞をactiveにするとき
  $globalmenu_myuma="active"     My馬をactiveにするとき
  $globalmenu_amu="active"       アミューズメントをactiveにするとき
  $globalmenu_seri="active"      せり情報をactiveにするとき
  $globalmenu_support="active"   サポートをactiveにするとき
  $globalnavi_top="active"       ナビのTopをactiveにするとき
  $globalnavi_race="active"      ナビのレース選択をactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalmenu_home) ? : $globalmenu_home = "" @endphp
@php isset($globalmenu_home) ? : $globalmenu_home = "" @endphp
@php isset($globalmenu_cyuou) ? : $globalmenu_cyuou = "" @endphp
@php isset($globalmenu_chihou) ? : $globalmenu_chihou = "" @endphp
@php isset($globalmenu_kaigai) ? : $globalmenu_kaigai = "" @endphp
@php isset($globalmenu_news) ? : $globalmenu_news = "" @endphp
@php isset($globalmenu_db) ? : $globalmenu_db = "" @endphp
@php isset($globalmenu_column) ? : $globalmenu_column = "" @endphp
@php isset($globalmenu_netpaper) ? : $globalmenu_netpaper = "" @endphp
@php isset($globalmenu_myuma) ? : $globalmenu_myuma = "" @endphp
@php isset($globalmenu_amu) ? : $globalmenu_amu = "" @endphp
@php isset($globalmenu_seri) ? : $globalmenu_seri = "" @endphp
@php isset($globalmenu_support) ? : $globalmenu_support = "" @endphp

<div id="menu">

  <div class="globalmenu">
    <ul class="">
      <li class="{{ $globalmenu_home }}"><a href="/index" title="HOME">HOME</a></li>
      <li class="{{ $globalmenu_cyuou }}"><a href="/cyuou_syutuba/201702050311" title="中央競馬">中央競馬</a></li>
      <li class="{{ $globalmenu_chihou }}"><a href="/chihou" title="地方競馬">地方競馬</a></li>
      <li class="{{ $globalmenu_kaigai }}"><a href="/kaigai" title="海外競馬">海外競馬</a></li>
      <li class="{{ $globalmenu_news }}"><a href="/news" title="ニュース">ニュース</a></li>
      <li class="{{ $globalmenu_db }}"><a href="/leading/leadingjockey" title="データ検索">データ検索</a></li>
      <li class="{{ $globalmenu_column }}"><a href="/weeklylog" title="コラム">コラム</a></li>
      <li class="{{ $globalmenu_netpaper }}"><a href="/netpaper" title="ネット新聞">ネット新聞</a></li>
      <li class="{{ $globalmenu_myuma }}"><a href="/myuma" title="My馬">My馬</a></li>
      <li class="{{ $globalmenu_amu }}"><a href="/amu" title="アミューズメント">アミューズメント</a></li>
      <li class="{{ $globalmenu_seri }}"><a href="/seri" title="せり情報">せり情報</a></li>
      <li class="{{ $globalmenu_support }}"><a href="/support" title="サポート">サポート</a></li>
    </ul>
  </div><!-- /.globalmenu -->

  {{-- 上記で選択されたメニュー別にナビを表示する --}}
  @if($globalmenu_cyuou == 'active')
    {{--  中央競馬のとき  --}}
    @if (isset($globalnavi_top))
    @elseif (isset($globalnavi_nittei))
    @elseif (isset($globalnavi_kensaku))
    @elseif (isset($globalnavi_member))
    @elseif (isset($globalnavi_win5))
    @elseif (isset($globalnavi_bunseki))
    @elseif (isset($globalnavi_seisai))
    @elseif (isset($globalnavi_tourokuba))
    @elseif (isset($globalnavi_masyou))
    @elseif (isset($globalnavi_best))
    @elseif (isset($globalnavi_syukusatu))
    @else
      {{-- default --}}
      @php $globalnavi_nittei = 'active' @endphp
    @endif
    @include('common.navicyuou')
  @elseif($globalmenu_chihou == 'active')
    {{-- 地方競馬のとき --}}
    @if (isset($globalnavi_top))
    @elseif (isset($globalnavi_nittei))
    @else
      {{-- default --}}
      @php $globalnavi_nittei = 'active' @endphp
    @endif
    @include('common.navichihou')
  @elseif($globalmenu_kaigai == 'active')
    {{-- 海外競馬のとき --}}
    @if (isset($globalnavi_top))
    @elseif (isset($globalnavi_nittei))
    @else
      {{-- default --}}
      @php $globalnavi_nittei = 'active' @endphp
    @endif
    @include('common.navikaigai')
  @elseif($globalmenu_db == 'active')
    {{-- データ検索のとき --}}
    @if (isset($globalnavi_uma))
    @elseif (isset($globalnavi_race))
    @elseif (isset($globalnavi_kisyu))
    @elseif (isset($globalnavi_cyokyosi))
    @elseif (isset($globalnavi_banusi))
    @elseif (isset($globalnavi_seisansya))
    @elseif (isset($globalnavi_leading))
    @else
      {{-- default --}}
      @php $globalnavi_leading = 'active' @endphp
    @endif
    @include('common.navidb')
  @elseif($globalmenu_netpaper == 'active')
    {{-- ネット新聞のとき --}}
    @if (isset($globalnavi_netpaper))
    @elseif (isset($globalnavi_noryoku))
    @else
      {{-- default --}}
      @php $globalnavi_netpaper = 'active' @endphp
    @endif
    @include('common.navinetpaper')
  @elseif($globalmenu_myuma == 'active')
    {{-- Ｍｙ馬のとき --}}
    @if (isset($globalnavi_myuma))
    @elseif (isset($globalnavi_okiniiri))
    @else
      {{-- default --}}
      @php $globalnavi_myuma = 'active' @endphp
    @endif
    @include('common.navimyuma')
  @elseif($globalmenu_amu == 'active')
    {{-- アミューズメントのとき --}}
    @if (isset($globalnavi_pog))
    @elseif (isset($globalnavi_g1))
    @else
      {{-- default --}}
      @php $globalnavi_amu = 'active' @endphp
    @endif
    @include('common.naviamu')
  @elseif($globalmenu_support == 'active')
    {{-- サポートのとき --}}
    @if (isset($globalnavi_qanda))
    @elseif (isset($globalnavi_otoiawase))
    @elseif (isset($globalnavi_kousin))
    @else
      {{-- default --}}
      @php $globalnavi_qanda = 'active' @endphp
    @endif
    @include('common.navisupport')
  @endif

</div><!-- /#menu -->
