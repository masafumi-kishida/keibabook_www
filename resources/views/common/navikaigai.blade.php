
{{--
  <<海外競馬用ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_top="active"          Topをactiveにするとき
  $globalnavi_nittei="active"       開催日程をactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_top) ? : $globalnavi_top = "" @endphp
@php isset($globalnavi_nittei) ? : $globalnavi_nittei = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_top }}"><a href="/" title="Top">Top</a></li>
      <li class="{{ $globalnavi_nittei }}"><a href="/" title="開催日程">開催日程</a></li>
    </ul>
  </div><!-- /.globalnavi -->
