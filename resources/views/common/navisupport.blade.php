
{{--
  <<サポート用ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_qanda="active"        よくある質問をactiveにするとき
  $globalnavi_otoiawase="active"    お問い合わせをactiveにするとき
  $globalnavi_kousin="active"       更新履歴をactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_qanda) ? : $globalnavi_qanda = "" @endphp
@php isset($globalnavi_otoiawase) ? : $globalnavi_otoiawase = "" @endphp
@php isset($globalnavi_kousin) ? : $globalnavi_kousin = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_qanda }}"><a href="/" title="よくある質問">よくある質問</a></li>
      <li class="{{ $globalnavi_otoiawase }}"><a href="/" title="お問い合わせ">お問い合わせ</a></li>
      <li class="{{ $globalnavi_kousin }}"><a href="/" title="更新履歴">更新履歴</a></li>
    </ul>
  </div><!-- /.globalnavi -->
