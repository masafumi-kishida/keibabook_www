
{{--
  <<データ検索用ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_uma="active"        馬をactiveにするとき
  $globalnavi_race="active"       レースをactiveにするとき
  $globalnavi_kisyu="active"      騎手をactiveにするとき
  $globalnavi_cyokyosi="active"   調教師をactiveにするとき
  $globalnavi_banusi="active"     馬主をactiveにするとき
  $globalnavi_seisansya="active"  生産者をactiveにするとき
  $globalnavi_leading="active"    リーディングをactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_uma) ? : $globalnavi_uma = "" @endphp
@php isset($globalnavi_race) ? : $globalnavi_race = "" @endphp
@php isset($globalnavi_kisyu) ? : $globalnavi_kisyu = "" @endphp
@php isset($globalnavi_cyokyosi) ? : $globalnavi_cyokyosi = "" @endphp
@php isset($globalnavi_banusi) ? : $globalnavi_banusi = "" @endphp
@php isset($globalnavi_seisansya) ? : $globalnavi_seisansya = "" @endphp
@php isset($globalnavi_leading) ? : $globalnavi_leading = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_uma }}"><a href="/" title="馬">馬</a></li>
      <li class="{{ $globalnavi_race }}"><a href="/" title="レース">レース</a></li>
      <li class="{{ $globalnavi_kisyu }}"><a href="/" title="騎手">騎手</a></li>
      <li class="{{ $globalnavi_cyokyosi }}"><a href="/" title="調教師">調教師</a></li>
      <li class="{{ $globalnavi_banusi }}"><a href="/" title="馬主">馬主</a></li>
      <li class="{{ $globalnavi_seisansya }}"><a href="/" title="生産者">生産者</a></li>
      <li class="{{ $globalnavi_leading }}"><a href="/leading/leadingjockey" title="リーディング">リーディング</a></li>
    </ul>
  </div><!-- /.globalnavi -->
