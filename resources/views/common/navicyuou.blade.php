
{{--
  <<中央競馬用ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_top="active"          Topをactiveにするとき
  $globalnavi_nittei="active"       開催日程をactiveにするとき
  $globalnavi_kensaku="active"      今週の出走馬検索をactiveにするとき
  $globalnavi_member="active"       今日の出走メンバーからをactiveにするとき
  $globalnavi_win5="active"         ＷＩＮ５をactiveにするとき
  $globalnavi_bunseki="active"      今日を分析するをactiveにするとき
  $globalnavi_seisai="active"       先週の制裁をactiveにするとき
  $globalnavi_tourokuba="active"    新規登録馬をactiveにするとき
  $globalnavi_masyou="active"       抹消馬をactiveにするとき
  $globalnavi_best="active"         ベスト調教をactiveにするとき
  $globalnavi_syukusatu="active"    縮刷版をactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_top) ? : $globalnavi_top = "" @endphp
@php isset($globalnavi_nittei) ? : $globalnavi_nittei = "" @endphp
@php isset($globalnavi_kensaku) ? : $globalnavi_kensaku = "" @endphp
@php isset($globalnavi_member) ? : $globalnavi_member = "" @endphp
@php isset($globalnavi_win5) ? : $globalnavi_win5 = "" @endphp
@php isset($globalnavi_bunseki) ? : $globalnavi_bunseki = "" @endphp
@php isset($globalnavi_seisai) ? : $globalnavi_seisai = "" @endphp
@php isset($globalnavi_tourokuba) ? : $globalnavi_tourokuba = "" @endphp
@php isset($globalnavi_masyou) ? : $globalnavi_masyou = "" @endphp
@php isset($globalnavi_best) ? : $globalnavi_best = "" @endphp
@php isset($globalnavi_syukusatu) ? : $globalnavi_syukusatu = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_top }}"><a href="/" title="Top">Top</a></li>
      <li class="{{ $globalnavi_nittei }}"><a href="/" title="開催日程">開催日程</a></li>
      <li class="{{ $globalnavi_kensaku }}"><a href="/cyuou_syutuba" title="今週の出走馬検索">今週の出走馬検索</a></li>
      <li class="{{ $globalnavi_member }}"><a href="/" title="今日の出走メンバーから">今日の出走メンバーから</a></li>
      <li class="{{ $globalnavi_win5 }}"><a href="/" title="ＷＩＮ５">ＷＩＮ５</a></li>
      <li class="{{ $globalnavi_bunseki }}"><a href="/" title="今日を分析する">今日を分析する</a></li>
      <li class="{{ $globalnavi_seisai }}"><a href="/" title="先週の制裁">先週の制裁</a></li>
      <li class="{{ $globalnavi_tourokuba }}"><a href="/" title="新規登録馬">新規登録馬</a></li>
      <li class="{{ $globalnavi_masyou }}"><a href="/" title="抹消馬">抹消馬</a></li>
      <li class="{{ $globalnavi_best }}"><a href="/" title="ベスト調教">ベスト調教</a></li>
      <li class="{{ $globalnavi_syukusatu }}"><a href="/" title="縮刷版">縮刷版</a></li>
    </ul>
  </div><!-- /.globalnavi -->
