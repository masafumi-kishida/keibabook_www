
{{--
  <<Ｍｙ馬ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_myuma="active"        馬をactiveにするとき
  $globalnavi_okiniiri="active"       レースをactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_myuma) ? : $globalnavi_myuma = "" @endphp
@php isset($globalnavi_okiniiri) ? : $globalnavi_okiniiri = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_myuma }}"><a href="/" title="Ｍｙ馬">Ｍｙ馬</a></li>
      <li class="{{ $globalnavi_okiniiri }}"><a href="/" title="お気に入り馬">お気に入り馬</a></li>
    </ul>
  </div><!-- /.globalnavi -->
