
{{--
  <<アミューズメント用ナビ>>
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $globalnavi_pog="active"      ＰＯＧをactiveにするとき
  $globalnavi_g1="active"       Ｇ１勝ち馬当てゲームをactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($globalnavi_pog) ? : $globalnavi_pog = "" @endphp
@php isset($globalnavi_g1) ? : $globalnavi_g1 = "" @endphp


  <div class="globalnavi">
    <ul class="">
      <li class="{{ $globalnavi_pog }}"><a href="/" title="ＰＯＧ">ＰＯＧ</a></li>
      <li class="{{ $globalnavi_g1 }}"><a href="/" title="Ｇ１勝ち馬当てゲーム">Ｇ１勝ち馬当てゲーム</a></li>
    </ul>
  </div><!-- /.globalnavi -->
