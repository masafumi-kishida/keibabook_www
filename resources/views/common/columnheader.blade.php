
{{--
  以下のように、このviewを呼び出す前に該当する変数をactiveにしておく
  $columnmenu_weekly="active"           ウィークリログをactiveにするとき
  $columnmenu_neraiuma="active"         今週の狙い馬をactiveにするとき
  $columnmenu_oikiritokusen="active"    日曜追い特選をactiveにするとき
  $columnmenu_oikirioikaichi="active"   追い切りピカイチをactiveにするとき
--}}
{{-- 変数がセットされているかチェック  --}}
@php isset($columnmenu_weekly) ? : $columnmenu_weekly = "" @endphp
@php isset($columnmenu_neraiuma) ? : $columnmenu_neraiuma = "" @endphp
@php isset($columnmenu_oikiritokusen) ? : $columnmenu_oikiritokusen = "" @endphp
@php isset($columnmenu_oikirioikaichi) ? : $columnmenu_oikirioikaichi = "" @endphp

<div id="menu">

  <div class="globalmenu">
    <ul class="">
      <li class="{{ $columnmenu_weekly }}"><a href="/weeklylog" title="ウィークリログ">ウィークリログ</a></li>
      <li class="{{ $columnmenu_neraiuma }}"><a href="/neraiuma" title="今週の狙い馬">今週の狙い馬</a></li>
      <li class="{{ $columnmenu_oikiritokusen }}"><a href="/oikiritokusen" title="日曜追い特選">日曜追い特選</a></li>
      <li class="{{ $columnmenu_oikirioikaichi }}"><a href="/oikiripikaichi" title="追い切りピカイチ">追い切りピカイチ</a></li>
    </ul>
    <ul class="">
      <li class="{{ $columnmenu_weekly }}"><a href="/weeklylog" title="ウィークリログ">ウィークリログ</a></li>
      <li class="{{ $columnmenu_neraiuma }}"><a href="/neraiuma" title="今週の狙い馬">今週の狙い馬</a></li>
      <li class="{{ $columnmenu_oikiritokusen }}"><a href="/oikiritokusen" title="日曜追い特選">日曜追い特選</a></li>
      <li class="{{ $columnmenu_oikirioikaichi }}"><a href="/oikiripikaichi" title="追い切りピカイチ">追い切りピカイチ</a></li>
    </ul>
    <ul class="">
      <li class="{{ $columnmenu_weekly }}"><a href="/weeklylog" title="ウィークリログ">ウィークリログ</a></li>
      <li class="{{ $columnmenu_neraiuma }}"><a href="/neraiuma" title="今週の狙い馬">今週の狙い馬</a></li>
      <li class="{{ $columnmenu_oikiritokusen }}"><a href="/oikiritokusen" title="日曜追い特選">日曜追い特選</a></li>
      <li class="{{ $columnmenu_oikirioikaichi }}"><a href="/oikiripikaichi" title="追い切りピカイチ">追い切りピカイチ</a></li>
    </ul>
  </div><!-- /.globalmenu -->

</div>
