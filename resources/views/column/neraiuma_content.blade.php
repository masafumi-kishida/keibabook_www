<?php
use App\hw_BUNSYO_MASTER_DB;
use App\TantousyaSettei;

echo "<P>",$genkoumei,"</p>";         // 原稿名表示
// デバッグ用////////////////////////////////////////////////////////////////////////
$entryd = array('genkoumei2' => '田中淳彦','genkoumei3' => 'ナリタハリケーン','bunsyo' => '（札幌・３歳未勝利）２日目４Ｒ芝１２００ｍ戦を予定。ゲートのタイミングが合わなかった前々走でも、終いに伸びて３着。発馬を決めた前走は正攻法から勝ちパターンだったが、相手が悪く２着。もうひと押しだが、どんな形になっても崩れず、今の未勝利なら力上位は明白。今週から札幌の芝はＣコースに。名手を背に今度こそ先行、押し切りを期待したい。');
/////////////////////////////////////////////////////////////////////////////////////
//
// 担当者設定テーブルより該当場所狙い馬担当ＴＭを出力順に取り込み
//
$entrys = TantousyaSettei::
  where('genkoumei1', $genkoumei)
  ->where('touzai', $bas)
  ->orderBy('jyunban', 'asc')
  ->get();
//--------------------------------------
// 1人分出力
//--------------------------------------
foreach ($entrys as $entry) {
  $bunsyo = null;
  // 該当ＴＭの"今週の狙い馬"を文書マスタより取り込み
  $bunsyo = hw_BUNSYO_MASTER_DB::where([
    ['syabetucd', '=', '10'],           // 週刊誌
    ['inputtm', '=', $entry->syozokucd.$entry->syainid],   // ＴＭコード
    ['genkoumei1', '=', $genkoumei],    // 原稿名
  ])
  //->get();
  ->value('bunsyoweb');                 // web用文章
  // 文書マスタの文章ありチェック
  if (!isset($bunsyo))
    continue;             // ==>次のＴＭ

  echo "<li>";
  if (isset($entry->link))
  {
    // 写真あり
    echo '<span class="image ">';
    echo '<img src="', $entry->link, '"';
    echo ' alt="Photo', $entry->hyouji1, '"></span>';
  }

  // ＴＭ名
  echo "<h2>", $entry->hyouji1;
  // 注釈(出張中)
  if (isset($entry->hyouji2))
  {
    echo "<span>", $entry->hyouji2, "</span>";
  }
  echo "</h2>";

  // 本文中の馬名にリンクを付加するサブコール
  //????::????($bunsyo);
  echo '<p>', $bunsyo,'</p>';
  /*
  echo '<p><a href="#">',$entryd['genkoumei3'],'</a>'; //ddddddddddddd
  echo $entryd['bunsyo'],'</p>';   //ddddddddddddddd
  */
  echo "</li>";
}

?>
