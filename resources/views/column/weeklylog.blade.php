{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  ウィークリーログ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/keibabook-www/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/keibabook-www/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/keibabook-www/css/global.css" rel="stylesheet" type="text/css">
  <link href="/keibabook-www/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- コラムactive --}}
  @php $globalmenu_column = "active" @endphp
  @include('common.header')
@endsection

{{-- ウィークリーログコンテンツ  --}}
@section('content')
  @php $columnmenu_weekly = "active" @endphp
  @include('common.columnheader')

@endsection
