{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  今週の狙い馬
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
<link href="/keibabook-www/css/reset.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/clear.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/global.css" rel="stylesheet" type="text/css">
<link href="/keibabook-www/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- コラムactive --}}
  <?php $globalmenu_column = "active" ?>
  @include('common.header')
@endsection

{{-- コラムコンテンツ  --}}
@section('content')
  {{-- 今週の狙い馬active --}}
  <?php $columnmenu_neraiuma = 'active' ?>
  @include('common.columnheader')

{{-- 狙い馬出力  --}}
<div class="tmsuisyou">
<h1> 今週の狙い馬 </h1>
<?php
  //isset($bas) ? : $bas = '0';   // 初期値 西ＴＭ
  //var_dump($bas);             ////////////var_dump
?>
<p><a href="/neraiuma/0">関西ＴＭ</a><br>
  <a href="/neraiuma/1">関東ＴＭ</a></p>

<!--
<?php
  //isset($_GET['bas']) ? $bas = $_GET['bas'] : $bas = '0';   // 初期値 西ＴＭ
?>
<p><a href="/neraiuma?bas=0">関西ＴＭ</a><br>
  <a href="/neraiuma?bas=1">関東ＴＭ</a></p>
-->

<?php $genkoumei = $bas == '1' ? "狙い馬(東ＴＭ)" : "狙い馬(西ＴＭ)"; ?>
<ul class="mainlist2">
  @include('column.neraiuma_content')
  {{--
    @include('column.neraiuma_content', ['one' => 7, 'two' => '3.1', 'three' => 5])
  --}}
</ul>
</div>
@endsection
