@php use App\Facades\Tableconvert @endphp
@php use Illuminate\Support\Facades\Redis @endphp

{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  heloテスト
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- 中央競馬active --}}
  @php $globalmenu_cyuou = "active" @endphp
  @php $globalnavi_kensaku = "active" @endphp
  @include('common.header')
@endsection

{{-- コンテンツ  --}}
@section('content')
  {{-- 出馬メニューのテスト --}}
  <?php
      $nen = '2017';
      $kai = '02';
      $basyocd = '05';
      $kaisai = '03';
      $race = '11';
  ?>
  @include('cyuou.syutubamenu')

  <p>{{ $message }}</p>
  <form method="post" action="/helo">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
  </form>
  <?php echo Tableconvert::nigesen('0'); ?>
  <?php echo Redis::get('sample'); ?>
@endsection
