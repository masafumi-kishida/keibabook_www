{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  ネット新聞　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- ネット新聞active --}}
  @php $globalmenu_netpaper = "active" @endphp
  @include('common.header')
@endsection

{{-- ネット新聞テストコンテンツ  --}}
@section('content')
  <div)
    <h1>ネット新聞テストコンテンツ</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/netpaper">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
