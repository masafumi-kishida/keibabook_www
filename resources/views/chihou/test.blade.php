{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  地方競馬　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- 地方競馬active --}}
  @php $globalmenu_chihou = "active" @endphp
  @include('common.header')
@endsection

{{-- 地方競馬テストコンテンツ  --}}
@section('content')
  <div)
    <h1>地方競馬コンテンツテスト</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/chihou">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
