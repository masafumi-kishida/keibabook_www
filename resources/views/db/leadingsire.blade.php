{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  heloテスト
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- 中央競馬active --}}
  @php $globalmenu_db = "active" @endphp
  @include('common.header')
@endsection

{{-- コンテンツ  --}}
@section('content')
<div id="leadingkiskyuinfo">
  <div class="select">
    <!-- ラジオボタンとプルダウンメニューを作り、選択された情報を受け取るシステム-->
    {{Form::open()}}
    <ul>
      <li class="right">
        表示年度を選択：
        {{Form::selectYear('year', date('Y'), 2004, 'nen')}}
        {{Form::submit('表示')}}
      </li>
    </ul>
    {{Form::close()}}
  </div><!-- /.nendoselect -->
  <div class="layout">
    <table class="example">
      <!-- 表の項目名表示 -->
      <thead>
        <tr>
          <th></th>
          <th>名前</th>
          <th>毛色</th>
          <th>JRA<br>登録頭数</th>
          <th>出走<br>頭数</th>
          <th>勝利<br>頭数</th>
          <th>勝馬率</th>
          <th>出走<br>回数</th>
          <th>勝利<br>回数</th>
          <th>勝率</th>
          <th>獲得<br>賞金</th>
          <th>一頭当<br>賞金</th>
          <th>一回当<br>賞金</th>
          <th>AEI</th>
          <th>平均勝距離<br>(芝)</th>
          <th>平均勝距離<br>(ダ)</th>
          <th>主な活躍馬</th>
        </tr>
      </thead>
      <?php echo $leadingkisyu_kansai;  ?>
    </table>
  </div>
</div>
@endsection
