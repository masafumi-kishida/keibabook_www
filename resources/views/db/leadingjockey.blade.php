{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  リーディングジョッキー
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- リーディングジョッキーative --}}
  @php $globalmenu_db = "active" @endphp
  @include('common.header')
@endsection

{{-- コンテンツ  --}}
@section('content')
<div id="leadingkiskyuinfo">
  <div class="select">
    <!-- ラジオボタンとプルダウンメニューを作り、選択された情報を受け取るシステム-->
    {{Form::open()}}
    <ul>
      <li class="right">
        表示年度を選択：
        {{Form::selectYear('year', date('Y'), 2004)}}
        {{Form::submit('表示')}}
      </li>
    </ul>
    {{Form::close()}}
  </div><!-- /.nendoselect -->
  <!------------------------------------------------------>
  <!--  東西リーディング表示                               -->
  <!------------------------------------------------------>
  <div class="layout">
    <!--西リーディング表示-->
    <div class="half">
      <table class="example">
        <caption>【関西】</caption>
        <!-- 表の項目名表示 -->
        <thead>
          <tr>
            <th></th>
            <th>名前</th>
            <th>1着</th>
            <th>2着</th>
            <th>3着</th>
            <th>着外</th>
            <th>勝率</th>
            <th>連対率</th>
            <th>通算</th>
          </tr>
        </thead>
        <!--   西リーディング一覧表作成 ($leadingkisyu_kansai)  -->
        <?php echo $leadingkisyu_kansai;  ?>

      </table>
    </div><!-- /.half -->

    <!--東についても同様-->
    <div class="half">
      <table class="example">
        <caption>【関東】</caption>
        <thead>
          <tr>
            <th></th>
            <th>名前</th>
            <th>1着</th>
            <th>2着</th>
            <th>3着</th>
            <th>着外</th>
            <th>勝率</th>
            <th>連対率</th>
            <th>通算</th>
          </tr>
        </thead>
        <?php echo $leadingkisyu_kantou;  ?>
      </table>
    </div><!-- /.half -->
  </div><!-- /.layout -->
</div><!-- /#leadingkiskyuinfo -->
@endsection
