<?php
//----------------------------------
// リーディングファイル読み込み
//----------------------------------
$entrys = array();
$query = DB::select('select * from leadingjt');
$query->where('clfflg', '0');
$query->where('nen', $nen);
$query->where('syubetu', $syubetu);
$query->where('touzai', $touzai);
$query->where_open();
$query->where('cyak1', '>', '0');
$query->or_where('cyak2', '>', '0');
$query->or_where('cyak3', '>', '0');
$query->or_where('cyak4', '>', '0');
$query->or_where('cyak5', '>', '0');
$query->or_where('cyakgai', '>', '0');
$query->where_close();
$query->order_by('cyak1', 'desc');
$query->order_by('cyak2', 'desc');
$query->order_by('cyak3', 'desc');
$query->order_by('cyak4', 'desc');
$query->order_by('cyak5', 'desc');
$query->order_by('cyakgai');
$entrys = $query->execute()->as_array();

$i = 1;
$j = 0;
// 人数分でループ
//-----------------------------
// 出馬１一覧表表示
//-----------------------------
foreach ($entrys as $entry) {
  if ($j > 0){
    $cyak1j = $cyak1;
    $cyak2j = $cyak2;
    $cyak3j = $cyak3;
    $cyakgaij = $cyakgai;
  }
  else{
    $cyak1j = 0;
    $cyak2j = 0;
    $cyak3j = 0;
    $cyakgaij = 0;
  }
  $name = $entry['name'];
  $hurdle = $entry['hurdle'];
  $cyak1 = $entry['cyak1'];
  $cyak2 = $entry['cyak2'];
  $cyak3 = $entry['cyak3'];
  $cyak4 = $entry['cyak4'];
  $cyak5 = $entry['cyak5'];
  $cyakgai = $entry['cyakgai'];
  $cyakgai = $cyak4 + $cyak5 + $cyakgai;
  if ($cyakgai != '0'){
    $syoritu = $cyak1 / ($cyak1 + $cyak2 + $cyak3 + $cyakgai);
    $renritu = ($cyak1 + $cyak2) / ($cyak1 + $cyak2 + $cyak3 + $cyakgai);
  }
  else{
    $syoritu = '.000';
    $renritu = '.000';
  }
  $total = $entry['total'];
  //$kcode = $entry['kcode'];

  $j = $j + 1;
  if ($cyak1 != $cyak1j || $cyak2 != $cyak2j || $cyak3 != $cyak3j || $cyakgai != $cyakgaij){
    $i = $j;
  }

  //----------------
  // 一人分のデータ作成
  //----------------
  echo '<tr>';
  //---------------------
  // 順位
  //---------------------
  echo '<td>'.$i.'</td>';
  //echo '<td>';
  //echo Html::anchor('/index.php/db/leadingjockey/'.$kcode.'/', $jtname.$hurdle);
  //echo '</td>';
  echo '<td>'.$name.$hurdle.'</td>';
  echo '<td align = "right">'.$cyak1.'</td>';
  echo '<td align = "right">'.$cyak2.'</td>';
  echo '<td align = "right">'.$cyak3.'</td>';
  echo '<td align = "right">'.$cyakgai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $renritu), 1).'</td>';
  echo '<td align = "right">'.$total.'</td>';
  echo '</tr>';
}


$entrys = array();
$query = DB::select()->from('leadingjt');
$query->where('clfflg', '0');
$query->where('nen', $nen);
$query->where('syubetu', $syubetu);
$query->where('touzai', $touzai);
$query->where('cyak1', '0');
$query->where('cyak2', '0');
$query->where('cyak3', '0');
$query->where('cyak4', '0');
$query->where('cyak5', '0');
$query->where('cyakgai', '0');
$query->order_by('total', 'desc');
$entrys = $query->execute()->as_array();

foreach ($entrys as $entry) {
  $name = $entry['name'];
  $hurdle = $entry['hurdle'];
  $cyak1 = $entry['cyak1'];
  $cyak2 = $entry['cyak2'];
  $cyak3 = $entry['cyak3'];
  $cyak4 = $entry['cyak4'];
  $cyak5 = $entry['cyak5'];
  $cyakgai = $entry['cyakgai'];
  $cyakgai = $cyak4 + $cyak5 + $cyakgai;
  $syoritu = '.000';
  $renritu = '.000';
  $total = $entry['total'];

  //----------------
  // 一人分のデータ作成
  //----------------
  echo '<tr>';
  //---------------------
  // 順位
  //---------------------
  echo '<td>'.'-'.'</td>';
  //echo '<td>';
  //echo Html::anchor('/index.php/syutuba2/index/'.$negahi.'/'.$keibajyocd.'/'.$race, $race."R");
  //echo '</td>';
  echo '<td>'.$name.$hurdle.'</td>';
  echo '<td align = "right">'.$cyak1.'</td>';
  echo '<td align = "right">'.$cyak2.'</td>';
  echo '<td align = "right">'.$cyak3.'</td>';
  echo '<td align = "right">'.$cyakgai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $renritu), 1).'</td>';
  echo '<td align = "right">'.$total.'</td>';
  echo '</tr>';
}
?>
