
<div id="leadingkiskyuinfo">
  <div class="select">

    <ul>
      <li class="right">
        <form method="post" action="/db/rankingseisansya/">
            {{ csrf_field() }}
            <select name="year">
              <option value="2017">選択肢1</option>
              <option value="2016">選択肢2</option>
              <option value="2015">選択肢3</option>
            </select>
            <input type="submit">
        </form>
      </li>
    </ul>

  </div><!-- /.nendoselect -->
  <div class="layout">
    <div class="half">
      <table class="example">
        <!-- 表の項目名表示 -->
        <thead>
          <tr>
            <th></th>
            <th>名前</th>
            <th>JRA<br>登録頭数</th>
            <th>出走<br>頭数</th>
            <th>勝利<br>頭数</th>
            <th>勝馬率</th>
            <th>出走<br>回数</th>
            <th>勝利<br>回数</th>
            <th>勝率</th>
            <th>獲得<br>賞金</th>
            <th>一頭当<br>賞金</th>
            <th>一回当<br>賞金</th>
            <th>AEI</th>
            <th>主な活躍馬</th>
          </tr>
        </thead>
        <?php echo $leadingkisyu_kansai;  ?>
      </table>
    </div><!-- /.half -->
  </div><!-- /.layout -->
</div><!-- /#leadingkiskyuinfo -->
