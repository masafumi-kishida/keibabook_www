<?php
//----------------------------------
// リーディング馬主生産者テーブル読み込み
//----------------------------------
$entrys = DB::table('leadingother')->where([
  ['clfflg', '=', '0'],
  ['syubetu', '=', $syubetu],
  ['nen', '=', $nen],
  ['syututou', '>', '0'],
])
->orderBy('syokin', 'desc')
->get();

$i = 1;
$j = 0;
// 人数分でループ
//-----------------------------
// リスト表示
//-----------------------------
foreach ($entrys as $entry) {
  if ($j > 0){
    $syokinj = $syokin;
  }
  else{
    $syokinj = 0;
  }
  $name = $entry->name;
  $touroku = $entry->touroku;
  $syututou = $entry->syututou;
  $katitou = $entry->katitou;
  $syutukai = $entry->syutukai;
  $katikai = $entry->katikai;
  $syokin = $entry->syokin;
  $aei = $entry->aei;
  $kbamei = $entry->kbamei;

  //計算(出走頭数、出走回数0は回避する、一応・・・)
  if ($syututou != '0') {
    $katiritu = $katitou / $syututou;
    $tousyokin = $syokin / $syututou;
  }
  else {
    $katiritu = '.000';
    $tousyokin = '0';
  }
  if ($syutukai != '0') {
    $syoritu = $katikai / $syutukai;
    $kaisyokin = $syokin / $syutukai;
  }
  else {
    $syoritu = '.000';
    $kaisyokin = '0';
  }

  $j = $j + 1;
  if ($syokin != $syokinj){
    $i = $j;
  }

  //----------------
  // 一人分のデータ作成う
  //----------------
  echo '<tr>';
  //---------------------
  // 順位
  //---------------------
  echo '<td>'.$i.'</td>';
  //echo '<td>';
  //echo Html::anchor('/index.php/syutuba2/index/'.$negahi.'/'.$keibajyocd.'/'.$race, $race."R");
  //echo '</td>';
  echo '<td>'.$name.'</td>';
  echo '<td align = "right">'.$touroku.'</td>';
  echo '<td align = "right">'.$syututou.'</td>';
  echo '<td align = "right">'.$katitou.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $katiritu), 1).'</td>';
  echo '<td align = "right">'.$syutukai.'</td>';
  echo '<td align = "right">'.$katikai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  echo '<td align = "right">'.number_format($syokin).'</td>';
  echo '<td align = "right">'.number_format($tousyokin).'</td>';
  echo '<td align = "right">'.number_format($kaisyokin).'</td>';
  echo '<td align = "right">'.$aei.'</td>';
  echo '<td align = "right">'.$kbamei.'</td>';
  echo '</tr>';
}

$entrys = DB::table('leadingother')->where([
  ['clfflg', '=', '0'],
  ['syubetu', '=', $syubetu],
  ['nen', '=', $nen],
  ['syututou', '=', '0'],
])
->get();

// 人数分でループ
//-----------------------------
// リスト表示
//-----------------------------
foreach ($entrys as $entry) {
  $name = $entry->name;
  $touroku = $entry->touroku;
  $syututou = $entry->syututou;
  $katitou = $entry->katitou;
  $syutukai = $entry->syutukai;
  $katikai = $entry->katikai;
  $syokin = $entry->syokin;
  $aei = $entry->aei;
  $kbamei = $entry->kbamei;
  $katiritu = '.000';
  $syoritu = '.000';
  $tousyokin = '0';
  $kaisyokin = '0';

  //----------------
  // 列作成
  //----------------
  echo '<tr>';
  //---------------------
  // 順位
  //---------------------
  echo '<td>'.'-'.'</td>';
  //echo '<td>';
  //echo Html::anchor('/index.php/syutuba2/index/'.$negahi.'/'.$keibajyocd.'/'.$race, $race."R");
  //echo '</td>';
  echo '<td>'.$name.'</td>';
  echo '<td align = "right">'.$touroku.'</td>';
  echo '<td align = "right">'.$syututou.'</td>';
  echo '<td align = "right">'.$katitou.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $katiritu), 1).'</td>';
  echo '<td align = "right">'.$syutukai.'</td>';
  echo '<td align = "right">'.$katikai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  echo '<td align = "right">'.number_format($syokin).'</td>';
  echo '<td align = "right">'.number_format($tousyokin).'</td>';
  echo '<td align = "right">'.number_format($kaisyokin).'</td>';
  echo '<td align = "right">'.$aei.'</td>';
  echo '<td align = "right">'.$kbamei.'</td>';
  echo '</tr>';
}
?>
