<?php
//----------------------------------
// リーディング種牡馬リスト読み込み(産駒が出走している馬)
//----------------------------------
$entrys = DB::table('leadingother')->where([
  ['clfflg', '=', '0'],
  ['syubetu', '=', $syubetu],
  ['nen', '=', $nen],
  ['syututou', '>', '0'],
])
->orderBy('syokin', 'desc')
->get();
$i = 1;
$j = 0;
// 頭数分でループ
foreach ($entrys as $entry) {
  //-----------------------------
  // 種牡馬データ
  //-----------------------------
  //順位比較用
  if ($j > 0){
    $syokinj = $syokin;
  }
  else{
    //1位用(syokinは後から取り込むため)
    $syokinj = 0;
  }

  //各種データ取り込み
  $name = $entry->name;
  $jkeiro = $entry->jkeiro;
  $touroku = $entry->touroku;
  $syututou = $entry->syututou;
  $katitou = $entry->katitou;
  $syutukai = $entry->syutukai;
  $katikai = $entry->katikai;
  $syokin = $entry->syokin;
  $aei = $entry->aei;
  $avesiba = $entry->avesiba;
  $avedirt = $entry->avedirt;
  $kbamei = $entry->kbamei;

  //計算(出走頭数、出走回数0は回避する、一応・・・)
  if ($syututou != '0') {
    $katiritu = $katitou / $syututou;
    $tousyokin = $syokin / $syututou;
  }
  else {
    $katiritu = '.000';
    $tousyokin = '0';
  }
  if ($syutukai != '0') {
    $syoritu = $katikai / $syutukai;
    $kaisyokin = $syokin / $syutukai;
  }
  else {
    $syoritu = '.000';
    $kaisyokin = '0';
  }


//順位処理。賞金が違えば別順位
  $j = $j + 1;
  if ($syokin != $syokinj){
    $i = $j;
  }

  //----------------
  // 1頭分の表示
  //----------------
  echo '<tr>';
  //---------------------
  // 順位
  //---------------------
  echo '<td>'.$i.'</td>';
  //リンク付き名前にする(・・・予定)
  //echo '<td>';
  //echo Html::anchor('/index.php/syutuba2/index/'.$negahi.'/'.$keibajyocd.'/'.$race, $race."R");
  //echo '</td>';
  echo '<td>'.$name.'</td>';
  echo '<td>'.$jkeiro.'</td>';
  echo '<td align = "right">'.$touroku.'</td>';
  echo '<td align = "right">'.$syututou.'</td>';
  echo '<td align = "right">'.$katitou.'</td>';
  //.XXX表示
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $katiritu), 1).'</td>';
  echo '<td align = "right">'.$syutukai.'</td>';
  echo '<td align = "right">'.$katikai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  //10,000円みたいな
  echo '<td align = "right">'.number_format($syokin).'</td>';
  echo '<td align = "right">'.number_format($tousyokin).'</td>';
  echo '<td align = "right">'.number_format($kaisyokin).'</td>';
  echo '<td align = "right">'.$aei.'</td>';
  echo '<td align = "right">'.number_format($avesiba).'</td>';
  echo '<td align = "right">'.number_format($avedirt).'</td>';
  echo '<td>'.$kbamei.'</td>';
  echo '</tr>';
}


//賞金なしパターン
$entrys = DB::table('leadingother')->where([
  ['clfflg', '=', '0'],
  ['syubetu', '=', $syubetu],
  ['nen', '=', $nen],
  ['syututou', '=', '0'],
])
->get();

// 頭数分でループ
foreach ($entrys as $entry) {
  $name = $entry->name;
  $jkeiro = $entry->jkeiro;
  $touroku = $entry->touroku;
  $syututou = $entry->syututou;
  $katitou = $entry->katitou;
  $syutukai = $entry->syutukai;
  $katikai = $entry->katikai;
  $syokin = $entry->syokin;
  $aei = $entry->aei;
  $kbamei = $entry->kbamei;
  //率などは全部0(出走していないので、エラーで落ちる)
    $katiritu = '.000';
    $syoritu = '.000';
    $tousyokin = '0';
    $kaisyokin = '0';
  $avesiba = '0';
  $avedirt = '0';

  echo '<tr>';
  //---------------------
  // 順位はなし
  //---------------------
  echo '<td>'.'-'.'</td>';
  //echo '<td>';
  //echo Html::anchor('/index.php/syutuba2/index/'.$negahi.'/'.$keibajyocd.'/'.$race, $race."R");
  //echo '</td>';
  echo '<td>'.$name.'</td>';
  echo '<td>'.$jkeiro.'</td>';
  echo '<td align = "right">'.$touroku.'</td>';
  echo '<td align = "right">'.$syututou.'</td>';
  echo '<td align = "right">'.$katitou.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $katiritu), 1).'</td>';
  echo '<td align = "right">'.$syutukai.'</td>';
  echo '<td align = "right">'.$katikai.'</td>';
  echo '<td align = "right">'.mb_substr(sprintf('%.3f', $syoritu), 1).'</td>';
  echo '<td align = "right">'.number_format($syokin).'</td>';
  echo '<td align = "right">'.number_format($tousyokin).'</td>';
  echo '<td align = "right">'.number_format($kaisyokin).'</td>';
  echo '<td align = "right">'.$aei.'</td>';
  echo '<td align = "right">'.number_format($avesiba).'</td>';
  echo '<td align = "right">'.number_format($avedirt).'</td>';
  echo '<td>'.$kbamei.'</td>';
  echo '</tr>';
}
?>
