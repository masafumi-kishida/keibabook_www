{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  データ検索　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- データ検索active --}}
  {{ $globalmenu_db = "active" }}
  @include('common.header')
@endsection

{{-- データ検索テストコンテンツ  --}}
@section('content')
  <div)
    <h1>データ検索テストコンテンツ</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/db">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
