<div id="pagetitle">
  <div class="title">
    <p>リーディング</p>
  </div>
</div>
<div id="leadingkiskyuinfo">
  <div class="select">
    <!-- ラジオボタンとプルダウンメニューを作り、選択された情報を受け取るシステム-->
    <form method="get" action = "" get>
      <ul>
        <li class="right">
          表示年度を選択：
                                        <?php
                     Form::start();
           Form::selectYear('year', 2013, 2015);
                              Form::close();

          echo "<select name= \"nen\">";
          echo   "<option value= \"2013\" >" . 2013 ."</option>\n";
          echo   "<option value= \"2014\" >" . 2014 ."</option>\n";
          echo   "<option value= \"2015\" >" . 2015 ."</option>\n";
          echo "</select>";
          ?>
        </li>
      </ul>
    </form>
  </div><!-- /.nendoselect -->
  <!------------------------------------------------------>
  <!--  東西リーディング表示                               -->
  <!------------------------------------------------------>
  <div class="layout">
    <!--西リーディング表示-->
  <div class="half">
    <table class="example">
      <caption>【関西】</caption>
      <!-- 表の項目名表示 -->
      <thead>
        <tr>
          <th></th>
          <th>名前</th>
          <th>1着</th>
          <th>2着</th>
          <th>3着</th>
          <th>着外</th>
          <th>勝率</th>
          <th>連対率</th>
          <th>通算</th>
        </tr>
      </thead>
      <!--   西リーディング一覧表作成 ($leadingkisyu_kansai)  -->
      <?php echo $leadingkisyu_kansai;  ?>

    </table>
  </div><!-- /.half -->

  <!--東についても同様-->
  <div class="half">
    <table class="example">
      <caption>【関東】</caption>
      <thead>
        <tr>
          <th></th>
          <th>名前</th>
          <th>1着</th>
          <th>2着</th>
          <th>3着</th>
          <th>着外</th>
          <th>勝率</th>
          <th>連対率</th>
          <th>通算</th>
        </tr>
      </thead>
      <?php echo $leadingkisyu_kantou;  ?>
    </table>
  </div><!-- /.half -->
</div><!-- /.layout -->
</div><!-- /#leadingkiskyuinfo -->
