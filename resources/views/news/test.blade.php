{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  ニュース　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- ニュースactive --}}
  @php $globalmenu_news = "active" @endphp
  @include('common.header')
@endsection

{{-- ニューステストコンテンツ  --}}
@section('content')
  <div)
    <h1>ニューステストコンテンツ</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/news">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
