<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta author="raru">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      @yield('title')
    </title>
    @yield('scripts')
    @yield('stylesheet')
  </head>
  <body>
    @yield('header')

    @yield('content')

    @yield('footer')

    @yield('side')
  </body>
</html>
