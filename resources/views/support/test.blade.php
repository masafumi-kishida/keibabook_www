{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  サポート　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- サポートactive --}}
  @php $globalmenu_support = "active" @endphp
  @include('common.header')
@endsection

{{-- サポートテストコンテンツ  --}}
@section('content')
  <div)
    <h1>サポートテストコンテンツ</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/support">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
