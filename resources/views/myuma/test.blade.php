{{-- マスターテンプレート継承  --}}
@extends('layouts.master')

{{--  タイトル  --}}
@section('title')
  Ｍｙ馬　テストコンテンツ
@endsection

{{-- javaスクリプト  --}}
@section('scripts')
@endsection

{{-- スタイルシート  --}}
@section('stylesheet')
  <link href="/css/reset.css" rel="stylesheet" type="text/css">
  <link href="/css/clear.css" rel="stylesheet" type="text/css">
  <link href="/css/global.css" rel="stylesheet" type="text/css">
  <link href="/css/import.css" rel="stylesheet" type="text/css">
@endsection

{{--  各コンテンツ共通メニュー  --}}
@section('header')
  {{-- Ｍｙ馬active --}}
  @php $globalmenu_myuma = "active" @endphp
  @include('common.header')
@endsection

{{-- Ｍｙ馬テストコンテンツ  --}}
@section('content')
  <div)
    <h1>Ｍｙ馬テストコンテンツ</h1>
    <p>{{ $message }}</p>
    <form method="post" action="/myuma">
      {{ csrf_field() }}
      <input type="text" name="str">
      <input type="submit">
    </form>
  </div>
@endsection
